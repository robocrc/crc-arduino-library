//Comment this when trying to use intellisense in visual micro
#include <CrcLib.h>
//------------------------------------------------------------

#define FL_MOTOR CRC_PWM_6
#define FR_MOTOR CRC_PWM_7
#define BL_MOTOR CRC_PWM_11
#define BR_MOTOR CRC_PWM_12

ColorDuration pattern[] = { { 2500, Color(213, 4, 200) }, { 1000, BLUE_LOW }, ColorDuration::END };
Note notes[] = { { 250, NOTE_B4 }, { 500, NOTE_SILENCE }, Note::END };

CrcLib::Timer timer;
void setup() {
  // put your setup code here, to run once:

    CrcLib::Initialize(false);
    Serial.begin(115200);
    Serial.println("Debug started");

    CrcLib::InitializePwmOutput(FL_MOTOR, false);
    CrcLib::InitializePwmOutput(FR_MOTOR, false);
    CrcLib::InitializePwmOutput(BL_MOTOR, false);
    CrcLib::InitializePwmOutput(BR_MOTOR, false);
    TestBinding();
    timer.Start(3000);
}

uint16_t last = millis();
void loop() {
  // put your main code here, to run repeatedly:
  static bool on = false;
  static uint8_t step = 0;
  
  CrcLib::Update();
  if (timer.IsFinished() && step == 0) {
    //CrcLib::PlayTune(notes, true);
    CrcLib::ShowColorPattern(pattern, true);
    step++;
    timer.Start(2000);
  }
  if (timer.IsFinished() && step == 1) {
    //TestBadDigitalPinWrite();
  }
  uint16_t now = millis();
  if (((uint16_t)(now - last)) >= 10) {
      last = now;
      /*Serial.print(now);
      Serial.print(" (");
      Serial.print(time - last);
      Serial.println(" delay)");
      // Serial.print(" => ");*/
      CrcLib::PrintControllerState();
      digitalWrite(CRC_DIG_1, CrcLib::IsCommValid());
  }
  bool com = CrcLib::IsCommValid();
  if (com != on && !com) {
      //TestBadDigitalPinWrite();
  }
  on = com;

  //TestBadDigitalPinWrite();
  TestErrorOnButton();

  unsigned int deltaMillis = CrcLib::GetDeltaTimeMillis();
  //Serial.println(deltaMillis);
  CrcLib::MoveHolonomic(ANALOG::JOYSTICK1_Y, ANALOG::JOYSTICK2_X, ANALOG::JOYSTICK1_X, FL_MOTOR, BL_MOTOR, FR_MOTOR, BR_MOTOR);

  unsigned int deltaMicros = CrcLib::GetDeltaTimeMicros();
  //Serial.println(deltaMicros);

  //TestComms();
}


//---------- Test Functions ----------




void TestBadDigitalPinWrite()
{
	//obviously not a proper digital pin
    CrcLib::SetDigitalOutput(CRC_ANA_3, HIGH);
}

void TestErrorOnButton()
{
    if (CrcLib::ReadDigitalChannel(BUTTON::COLORS_UP))
        TestBadDigitalPinWrite();
}

void TestComms() 
{
    if (CrcLib::ReadDigitalChannel(BUTTON::L1)) {
        tone(46, 1000 + CrcLib::ReadAnalogChannel(ANALOG::JOYSTICK1_X));
    }
    else {
        noTone(46);
    }
}

void TestBinding()
{
    digitalWrite(CRC_DIG_3, CrcLib::ReadDigitalChannel(BUTTON::COLORS_DOWN));
    
    CrcLib::InitializePwmOutput(CRC_PWM_9);
    analogWrite(CRC_PWM_9, CrcLib::ReadAnalogChannel(ANALOG::JOYSTICK1_Y));
}

//------------------------------------
