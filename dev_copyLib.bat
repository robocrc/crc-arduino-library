rmdir /s /q "%USERPROFILE%\Documents\Arduino\libraries\CrcLib"
robocopy "%~dp0\LibraryTestSketch\CrcLib" "%USERPROFILE%\Documents\Arduino\libraries\CrcLib" /mir

rmdir /s /q "%USERPROFILE%\Documents\Arduino\libraries\CrcDependency_xbee-arduino-master"
robocopy "%~dp0\LibraryTestSketch\Dependencies\CrcDependency_xbee-arduino-master" "%USERPROFILE%\Documents\Arduino\libraries\CrcDependency_xbee-arduino-master" /mir

rmdir /s /q "%USERPROFILE%\Documents\Arduino\libraries\CrcDependency_Adafruit_NeoPixel-master"
robocopy "%~dp0\LibraryTestSketch\Dependencies\CrcDependency_Adafruit_NeoPixel-master" "%USERPROFILE%\Documents\Arduino\libraries\CrcDependency_Adafruit_NeoPixel-master" /mir

end