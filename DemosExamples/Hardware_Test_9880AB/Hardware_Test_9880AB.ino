//This program allows to easily test the solderings of components on the board.
//Ground the pins and check if the corresponding text prints on the serial monitor.
//Can also be quickly tested using the harware tester mini piggy board
//Status and Fail LED flash when this program runs.

#include <CrcLib.h>

unsigned int x = 0;

void setup() {

  CrcLib::Initialize();
  pinMode(CRC_PWM_1, OUTPUT);
  pinMode(CRC_PWM_2, OUTPUT);
  pinMode(CRC_PWM_3, OUTPUT);
  pinMode(CRC_PWM_4, OUTPUT);
  pinMode(CRC_PWM_5, OUTPUT);
  pinMode(CRC_PWM_6, OUTPUT);
  pinMode(CRC_PWM_7, OUTPUT);
  pinMode(CRC_PWM_8, OUTPUT);
  pinMode(CRC_PWM_9, OUTPUT);
  pinMode(CRC_PWM_10, OUTPUT);
  pinMode(CRC_PWM_11, OUTPUT);
  pinMode(CRC_PWM_12, OUTPUT);

  pinMode(CRC_SPI_MISO, OUTPUT);
  pinMode(CRC_SPI_MOSI, OUTPUT);
  pinMode(CRC_SPI_SCK, OUTPUT);
  pinMode(CRC_SPI_SS, OUTPUT);

  pinMode(21, OUTPUT);
  pinMode(19, OUTPUT);
  pinMode(3, OUTPUT);

  pinMode(34, OUTPUT);
  pinMode(39, OUTPUT);

  Serial.begin(9600);
  reset();
  digitalWrite(CRC_PWM_1, LOW); delay(40); reset();
  digitalWrite(CRC_PWM_2, LOW); delay(40); reset();
  digitalWrite(CRC_PWM_3, LOW); delay(40); reset();
  digitalWrite(CRC_PWM_4, LOW); delay(40); reset();
  digitalWrite(CRC_PWM_5, LOW); delay(40); reset();
  digitalWrite(CRC_PWM_6, LOW); delay(40); reset();
  digitalWrite(CRC_PWM_7, LOW); delay(40); reset();
  digitalWrite(CRC_PWM_8, LOW); delay(40); reset();
  digitalWrite(CRC_PWM_9, LOW); delay(40); reset();
  digitalWrite(CRC_PWM_10, LOW); delay(40); reset();
  digitalWrite(CRC_PWM_11, LOW); delay(40); reset();
  digitalWrite(CRC_PWM_12, LOW); delay(40); reset();

  digitalWrite(CRC_SPI_MISO, LOW); delay(40); reset();
  digitalWrite(CRC_SPI_MOSI, LOW); delay(40); reset();
  digitalWrite(CRC_SPI_SCK, LOW); delay(40); reset();
  digitalWrite(CRC_SPI_SS, LOW); delay(40); reset();

  digitalWrite(CRC_I2C_B, LOW); delay(40); reset();
  digitalWrite(CRC_SERIAL_B, LOW); delay(40); reset();
  digitalWrite(CRC_ENCO_B, LOW); delay(40); reset();

  digitalWrite(34, HIGH); delay(40); reset();//Status
  digitalWrite(39, HIGH); delay(40); reset();//Fail
  reset();

  analogWrite(46,0);
}

void loop() {
  // put your main code here, to run repeatedly:
CrcLib::Update();

  if (x)
  {
    x--;
  }
  else
  {
    x = 1500;
    digitalWrite(34, !digitalRead(34)); //Makes Status and Fail flash
    digitalWrite(39, !digitalRead(34)); //Makes Status and Fail flash
    Serial.println("----------------------------------------------------------");
    Serial.println(CrcLib::GetBatteryVoltage(1.01));
  }


  if (CrcLib::GetDigitalInput(CRC_DIG_1)) {

    digitalWrite(CRC_PWM_1, LOW);
  } else {
    digitalWrite(CRC_PWM_1, HIGH); Serial.println(1);
  }
  if (CrcLib::GetDigitalInput(CRC_DIG_2)) {

    digitalWrite(CRC_PWM_2, LOW);
  } else {
    Serial.println(2);
    digitalWrite(CRC_PWM_2, HIGH);
  }
  if (CrcLib::GetDigitalInput(CRC_DIG_3)) {
    digitalWrite(CRC_PWM_3, LOW);
  } else {
    Serial.println(3);
    digitalWrite(CRC_PWM_3, HIGH);
  }
  if (CrcLib::GetDigitalInput(CRC_DIG_4)) {
    digitalWrite(CRC_PWM_4, LOW);
  } else {
    Serial.println(4);
    digitalWrite(CRC_PWM_4, HIGH);
  }
  if (CrcLib::GetDigitalInput(CRC_DIG_5)) {
    digitalWrite(CRC_PWM_5, LOW);
  } else {
    Serial.println(5);
    digitalWrite(CRC_PWM_5, HIGH);
  }
  if (CrcLib::GetDigitalInput(CRC_DIG_6)) {
    digitalWrite(CRC_PWM_6, LOW);
  } else {
    Serial.println(6);
    digitalWrite(CRC_PWM_6, HIGH);
  }
  if (CrcLib::GetDigitalInput(CRC_DIG_7)) {
    digitalWrite(CRC_PWM_7, LOW);
  } else {
    Serial.println(7);
    digitalWrite(CRC_PWM_7, HIGH);
  }
  if (CrcLib::GetDigitalInput(CRC_DIG_8)) {
    digitalWrite(CRC_PWM_8, LOW);
  } else {
    Serial.println(8);
    digitalWrite(CRC_PWM_8, HIGH);
  }
  if (CrcLib::GetDigitalInput(CRC_DIG_9)) {
    digitalWrite(CRC_PWM_9, LOW);
  } else {
    Serial.println(9);
    digitalWrite(CRC_PWM_9, HIGH);
  }
  if (CrcLib::GetDigitalInput(CRC_DIG_10)) {
    digitalWrite(CRC_PWM_10, LOW);
  } else {
    Serial.println(10);
    digitalWrite(CRC_PWM_10, HIGH);
  }
  if (CrcLib::GetDigitalInput(CRC_DIG_11)) {
    digitalWrite(CRC_PWM_11, LOW);
  } else {
    Serial.println(11);
    digitalWrite(CRC_PWM_11, HIGH);
  }
  if (CrcLib::GetDigitalInput(CRC_DIG_12)) {
    digitalWrite(CRC_PWM_12, LOW);
  } else {
    Serial.println(12);
    digitalWrite(CRC_PWM_12, HIGH);
  }

  if (CrcLib::GetAnalogInput(CRC_ANA_1)) {
    digitalWrite(CRC_SPI_MISO, LOW);
  } else {
    Serial.println("ANA_1");
    digitalWrite(CRC_SPI_MISO, HIGH);
  }
  if (CrcLib::GetAnalogInput(CRC_ANA_2)) {
    digitalWrite(CRC_SPI_MOSI, LOW);
  } else {
    Serial.println("ANA_2");
    digitalWrite(CRC_SPI_MOSI, HIGH);
  }
  if (CrcLib::GetAnalogInput(CRC_ANA_3)) {
    digitalWrite(CRC_SPI_SCK, LOW);
  } else {
    digitalWrite(CRC_SPI_SCK, HIGH);
    Serial.println("ANA_3");
  }
  if (CrcLib::GetAnalogInput(CRC_ANA_4)) {
    digitalWrite(CRC_SPI_SS, LOW);
  } else {
    Serial.println("ANA_4");
    digitalWrite(CRC_SPI_SS, HIGH);
  }

  if (digitalRead(CRC_I2C_A)) {
    digitalWrite(CRC_I2C_B, LOW);
  } else {
    Serial.println("I2C_A");
    digitalWrite(CRC_I2C_B, HIGH);
  }
  if (digitalRead(CRC_SERIAL_A)) {
    digitalWrite(CRC_SERIAL_B, LOW);
  } else {
    Serial.println("SERIAL_A");
    digitalWrite(CRC_SERIAL_B, HIGH);
  }
  if (digitalRead(CRC_ENCO_A)) {

    digitalWrite(CRC_ENCO_B, LOW);
  } else {
    Serial.println("ENCO_A");
    digitalWrite(CRC_ENCO_B, HIGH);
  }

}


//******************
void reset() {
  digitalWrite(CRC_PWM_1, HIGH);
  digitalWrite(CRC_PWM_2, HIGH);
  digitalWrite(CRC_PWM_3, HIGH);
  digitalWrite(CRC_PWM_4, HIGH);
  digitalWrite(CRC_PWM_5, HIGH);
  digitalWrite(CRC_PWM_6, HIGH);
  digitalWrite(CRC_PWM_7, HIGH);
  digitalWrite(CRC_PWM_8, HIGH);
  digitalWrite(CRC_PWM_9, HIGH);
  digitalWrite(CRC_PWM_10, HIGH);
  digitalWrite(CRC_PWM_11, HIGH);
  digitalWrite(CRC_PWM_12, HIGH);
  digitalWrite(CRC_SPI_MISO, HIGH);
  digitalWrite(CRC_SPI_MOSI, HIGH);
  digitalWrite(CRC_SPI_SCK, HIGH);
  digitalWrite(CRC_SPI_SS, HIGH);
  digitalWrite(CRC_I2C_B, HIGH);
  digitalWrite(CRC_SERIAL_B, HIGH);
  digitalWrite(CRC_ENCO_B, HIGH);
  digitalWrite(34, LOW); //Status
  digitalWrite(39, LOW); //Fail

}
