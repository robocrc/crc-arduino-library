#include <CrcLib.h>

#define LEFT_MOTOR CRC_PWM_5
#define RIGHT_MOTOR CRC_PWM_6

uint8_t leftJoystickY = 0;
uint8_t rightJoystickX = 0;

//Function prototype for the custom function written below
void MyCoolFunction(unsigned char pin, BUTTON button1, BUTTON button2);

//Run once on start
void setup() {

  CrcLib::Initialize();

  //Initialize PWM outputs for motors, the second one is reversed
  CrcLib::InitializePwmOutput(LEFT_MOTOR);
  CrcLib::InitializePwmOutput(RIGHT_MOTOR, true);

  CrcLib::SetDigitalPinMode(CRC_DIG_4, OUTPUT);

  //Bind PWM pin 9 to the d-pad down button
  //This will set the pin to be automatically updated
  CrcLib::BindPinToButton(CRC_PWM_9, BUTTON::DOWN);
}

//Runs continuously in a loop
void loop() {

  //Update outputs and controller values
  CrcLib::Update();

  leftJoystickY = CrcLib::ReadAnalogChannel(ANALOG::LCHANY);
  rightJoystickX = CrcLib::ReadAnalogChannel(ANALOG::RCHANX);

  //Set motor values using the two wheele arcade scheme
  //Left joystick y as forward/back, right joystick x as rotate
  //Left motor as defined above is on PWM pin 5 and right is on PWM pin 6
  CrcLib::Arcade(leftJoystickY, rightJoystickX, LEFT_MOTOR, RIGHT_MOTOR);

  //Call the custom function with digital pin 4, and buttons L1/L2
  MyCoolFunction(CRC_DIG_4, BUTTON::L1, BUTTON::L2)
}

//Manually activate a given digital pin when either one of two buttons is pressed
void MyCoolFunction(unsigned char pin, BUTTON button1, Button button2)
{
  if(CrcLib::ReadDigitalChannel(button1) || CrcLib::ReadDigitalChannel(button2))
  {
    CrcLib::SetDigitalOutput(pin, HIGH);
  }
  else
  {
    CrcLib::SetDigitalOutput(pin, LOW);
  }
}
