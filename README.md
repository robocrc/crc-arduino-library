# arduino-lib
Repo for the ardunio project.

This repo will remain private as it contains tools and a setup used for development. In terms of distribution and documentation for the kids, we'll have to discuss various solutions.

## General
The library and dependencies all should go separately in the arduino IDE's library folder.

This means you can transfer them manually, or use the dev_copyLib.bat file to copy them over automatically if you're on windows.

Compiling should ultimately be tested with the arduino ide since it seems to be extra picky and will most likely be what the kids use.

## Dev - Build pipeline
- If you've just cloned the repo or you're sure the libraries don't existin the arduino's libraries folder, it's a good idea to run dev_copyLib.bat once before doing anything else.
- Edit the cpp/h/ino files using whatever editor you want. (If you're using visual micro in vs, then you might run into an issue with the includes, in which case just follow the instructions at the top of LibraryTestSketch.ino)
- When you want to test, run dev_copyLib.bat. This will copy the CrcLib and all the dependencies to the arduino's default libraries folder. You can also do this step manually if you're not on windows or the script doesn't work for some reason.
- Open the test .ino file in the arduino ide. Compile/run from there.

## Coding Guidelines
- General C++ coding guidelines apply in terms of naming conventions.
- For the sake of consistency, curly braces will go on the line below the function declaration.
- Proper indentation should be maintained, the standard will be tabs as spaces, aka a tab should be 4 spaces. This matters only for our own .cpp and .h files, not so much for the .ino file. Make sure that whatever editor you're using to edit our library files is set to input spaces when you click tab.
- Documentation is cool and all but we'll leave the formal docs for later. For now, during development just put comments where you think they're needed.

## Git Guidelines
- Make sure your git is set up properly (you have a name/email associated with it and any other configuration that's needed).
- Always work on branches, never commit directly to master.
- Generally only one person should ever push to any given branch, although a branch can be checked out by multiple people.
- Small commits are better, it makes things easier to understand and change if need be when a single commit kind of encompases a single idea and isn't too huge.
- Small pull requests are better as well! This make is easier to continue with development and not get stuck on massive changes which will block everybody.
- Try to avoid commits that change stuff without changing things (adding a random empty line, removing a whitespace here or there) unless the putpose of the commit or the PR is to specifically clean stuff up
- There's no shame in creating a new branch and getting rid of an old one. If you've made something but gotten to where you are now through a really messy path then it's better to create a new branh and redo it cleaner. The reason for this is that in order for git to get to the current state of the repo, it runs through every change from the beginning! That means that if we have any messy history it'll be forced to run through it as well and that can slow down the process and bloat the repo.  


Test link yo our [wiki](https://github.com/robo-crc/arduino-lib-dev/wiki).
