let
  pkgs = import <nixpkgs> {};
in
with pkgs;

stdenv.mkDerivation {
  name = "env";

  ARDUINO_PATH="${pkgs.arduino-core}";

  buildInputs = [
    arduino openocd usbutils gdb gcc-arm-embedded dfu-util bossa clang-tools screen
  ];

  shellHook = ''
    export PATH="$PATH:${pkgs.arduino-core}/share/arduino/"
  '';
}

