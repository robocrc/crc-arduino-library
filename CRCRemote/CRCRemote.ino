/**
 * Copyright (c) 2009 Andrew Rapp. All rights reserved.
 *
 * This file is part of XBee-Arduino.
 *
 * XBee-Arduino is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * XBee-Arduino is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XBee-Arduino.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <XBee.h>

#define rumbleMotorPin 2
#define RxLEDPin 17

//xBee Destination Addresses (DH and DL adresses in the xBee's Config! or SH and SL of correspong robot xBee!)
#define DHADDRESS 0x0013A200
#define DLADDRESS 0x4198A64A

//Analog Joystick mapping 
#define LCHANX A5
#define LCHANY A4
#define RCHANX A3
#define RCHANY A2

//Baud rate for serial - Make sure xBee is configured accordingly!
#define BAUDRATE 115200

//Maps to each bit of byte 0 of payload
enum btnMap0
{
    SELECT, START, L1, L2, L3, R1, R2, R3
};

//Maps to each bit of byte 1 of payload
enum btnMap1 
{
    UP, RIGHT, DOWN, LEFT, GREENONE, REDTWO, BLUETHREE, PINKFOUR
};

//Function prototypes
void initIO();
void readRemote(uint8_t * data);
void printPayload();
void validateReception();


//XBee object that manages sending data
XBee xbee = XBee();

//Data payload for xBee
//Structure:
//Byte 0 - View btnMap0 (0-1 for each individual input)
//Byte 1 - View btnMap1 (0-1 for each individual input)
//Byte 2 - Left Joystick X Channel  (0-255)
//Byte 3 - Left Joystick Y Channel  (0-255)
//Byte 4 - Right Joystick X Channel (0-255)
//Byte 5 - Right Joystick Y Channel (0-255)
uint8_t payload[] = {0,0,0,0,0,0};

// SH + SL Address of receiving XBee
XBeeAddress64 addr64 = XBeeAddress64(DHADDRESS, DLADDRESS);

// xBee status management
ZBTxRequest zbTx = ZBTxRequest(addr64, payload, sizeof(payload));
ZBTxStatusResponse txStatus = ZBTxStatusResponse();

//Whether or not the last send produced a valid response
bool hadValidResponse = true;

//How often to poll in milliseconds if we're
//not getting valid responses from the robot
unsigned long pollTime = 2000;

//Time in milliseconds of the last send attempt
unsigned long lastSendTime = 0;
//TIme since last sent
unsigned long timeSinceLast = 0;

void setup() 
{
  initIO();
  Serial.begin(BAUDRATE);
  Serial1.begin(BAUDRATE);
  xbee.setSerial(Serial1);
}

void loop() 
{
  timeSinceLast = millis() - lastSendTime;

  //Only send something if we know the robot has received out last send,
  //or if we've exceeded the polling time
  if(hadValidResponse || timeSinceLast > pollTime)
  {
    //Read current remote inputs and copy to payload
    readRemote(payload);
  
    //For debugging use only
    //printPayload();
    
    //Send payload via xBee
    xbee.send(zbTx);

    lastSendTime = millis();

    //Validate that the information was received
    validateReception();
  }
  else
  {
    Serial.println("Not sending - No valid response or haven't waited long enough to poll.");
  }
}


//Call this function once during setup()
//Function that sets each arduino pin to INPUT
//Turns off rumble and RXLED
void initIO()
{
    for (int i = 3; i < 17; i++)
    {
        pinMode(i, INPUT);
    }
    pinMode(rumbleMotorPin, OUTPUT);
    pinMode(RxLEDPin, OUTPUT);
    digitalWrite(rumbleMotorPin, LOW);  // Stop rumbling of the gamepad
    digitalWrite(RxLEDPin, HIGH); // LED off at beginning
}



//Reads remote Inputs and copies it to *data
//data must be 6 bytes or else it will crash
void readRemote(uint8_t * data)
{
    //Used for storing the channel value before masking it
    uint16_t temp{};

    //Direct Port reading is used to reduce read speeds by 90%

    //Start of Byte Zero-----------------------------------

    //Read Select button PD0/D3 - Add to bit 0
    bitWrite(data[0], SELECT, (~PIND & 0B00000001));

    //Read Start button PD4/D4- Add to bit 1
    bitWrite(data[0], START, (~PIND & 0B00010000));

    //Read L1
    bitWrite(data[0], L1, (~PINB & 0B00000010));

    //Read L2
    bitWrite(data[0], L2, (~PINB & 0B00000100));

    //Read L3 PF6/A0 (Wrong in specsheet)
    bitWrite(data[0], L3, (~PINF & 0B01000000));

    //Read R1
    bitWrite(data[0], R1, (~PINC & 0B10000000));

    //Read R2
    bitWrite(data[0], R2, (~PINB & 0B00001000));

    //Read R3 PF7/A1 (Wrong in specsheet)
    bitWrite(data[0], R3, (~PINF & 0B10000000));

    //End of Byte Zero---------------------------------------

    //Start of Byte 1 ---------------------------------------

    //Read Up 
    bitWrite(data[1], UP, (~PINC & 0B01000000));

    //Read Right
    bitWrite(data[1], RIGHT, (~PINB & 0B00010000));

    //Read Down
    bitWrite(data[1], DOWN, (~PINE & 0B01000000));

    //Read Left
    bitWrite(data[1], LEFT, (~PIND & 0B10000000));

    //Read GreenOne
    bitWrite(data[1], GREENONE, (~PINB & 0B00100000));

    //Read RedTwo
    bitWrite(data[1], REDTWO, (~PINB & 0B10000000));

    //Read BlueThree
    bitWrite(data[1], BLUETHREE, (~PIND & 0B01000000));

    //Read PinkFour
    bitWrite(data[1], PINKFOUR, (~PINB & 0B01000000));

    //End of Byte 1 ---------------------------------------

    //Start of Byte 2 -------------------------------------

    //Read Left Joystick X-axis
    temp = analogRead(LCHANX);
    //Take bits 2 to 9
    data[2] = (temp & 0B0000001111111100) >> 2;
    //End of Byte 2 ---------------------------------------

    //Start of Byte 3--------------------------------------

    //Read Left Joystick Y-axis
    temp = analogRead(LCHANY);
    //Take bits 2 to 9
    data[3] = (temp & 0B0000001111111100) >> 2;

    //End of Byte 3 ---------------------------------------

    //Start of Byte 4--------------------------------------
    //Read Right Joystick X-axis
    temp = analogRead(RCHANX);
    //Take bits 2 to 9
    data[4] = (temp & 0B0000001111111100) >> 2;
    //End of Byte 4 ---------------------------------------

    //Start of Byte 5--------------------------------------
    //Read Right Joystick Y-axis
    temp = analogRead(RCHANY);
    //Take bits 2 to 9
    data[5] = (temp & 0B0000001111111100) >> 2;
    //End of Byte 5 ---------------------------------------
}


//Function that prints to the serial port the current state of the payload
//For Debugging purposes only
//Remove from your code for release
void printPayload() {
    for (int b = 7; b >= 0; b--)
    {
        Serial.print(bitRead(payload[0], b));
    }
    Serial.println("");
    for (int b = 7; b >= 0; b--)
    {
        Serial.print(bitRead(payload[1], b));
    }
    Serial.println("");
    Serial.print("Left X: ");
    Serial.println(static_cast<uint16_t>(payload[2]));
    Serial.print("Left Y: ");
    Serial.println(static_cast<uint16_t>(payload[3]));
    Serial.print("Right X: ");
    Serial.println(static_cast<uint16_t>(payload[4]));
    Serial.print("Right Y: ");
    Serial.println(static_cast<uint16_t>(payload[5]));
}


//Function that validates that the payload was correctly sent
//Times out after 500 ms
//RX Led lights up if sent correctly
void validateReception() {
    //after sending a tx request, we expect a status response
    //wait up to half second for the status response
    //Serial.println("Sent");
    if (xbee.readPacket(500)) {
        // got a response!
        //Serial.println("Received");
        // should be a znet tx status                
        if (xbee.getResponse().getApiId() == ZB_TX_STATUS_RESPONSE) {
            xbee.getResponse().getZBTxStatusResponse(txStatus);

            // get the delivery status, the fifth byte
            if (txStatus.getDeliveryStatus() == SUCCESS) {
                //Set Status LED
                //Serial.println("Success");
                hadValidResponse = true;
                RXLED1;
            }
            else {
                // the remote XBee did not receive our packet. is it powered on?
                // Set Status LED - HIGH is off for some reason
                hadValidResponse = false;
                RXLED0;
            }
        }
    }
    else if (xbee.getResponse().isError()) {
        //nss.print("Error reading packet.  Error code: ");  
        //nss.println(xbee.getResponse().getErrorCode());
        hadValidResponse = false;
    }
    else {
        // local XBee did not provide a timely TX Status Response -- should not happen
        hadValidResponse = false;
    }
}
