#include "hidjoystickrptparser.h"
#include "joy_state.h"
#include "preparsed.h"

impl::CrcHIDParser::CrcHIDParser(format_t& format)
    : format(format)
{
}

void impl::CrcHIDParser::Parse(const uint32_t len,
    const uint8_t* pbuf,
    const uint32_t& offset __attribute__((unused)))
{
    valid = parse_format((uint8_t*)pbuf, len, &format);
}

bool impl::CrcHIDParser::is_valid() { return valid; }

impl::JoystickReportParser::JoystickReportParser(format_t& format)
    : _state({ 0 })
    , format(format)
    , _ready(false)
{
}

void impl::JoystickReportParser::ready() { _ready = true; }

bool impl::JoystickReportParser::isReady() { return _ready; }

CrcUtility::RemoteState impl::JoystickReportParser::State() { return _state; }

void impl::JoystickReportParser::Parse(
    HID* hid, uint32_t is_rpt_id, uint32_t len, uint8_t* buf)
{
    if (!_ready)
        return;

    read_state(_state, format, buf, len);
}

uint32_t HIDJoystick::isReady()
{
    return HIDUniversal::isReady() && parser.isReady();
}

CrcUtility::RemoteState HIDJoystick::State() { return parser.State(); }

uint32_t HIDJoystick::OnInitSuccessful()
{
    uint8_t rcode = 0;
    if (VID == impl::LOGITECH_VID && PID == impl::LOGITECH_PID) {
        format = F130_descr();
    } else if (VID == impl::HAVIT_VID && PID == impl::HAVIT_PID) {
        format = havit_descr();
    } else {
        impl::CrcHIDParser HIDParse(format);
        rcode = GetReportDescr(0, &HIDParse);
        if (!HIDParse.is_valid()) {
            rcode = 1;
        }
    }

    if (rcode != 0) {
        Release();
    } else {
        parser.ready();
        SetReportParser(format.use_reports ? 1 : 0,
            &parser); // For now, only reports for PS3
    }

    return rcode;
}
