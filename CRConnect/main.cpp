#include "hidparser.h"
#include "joy_state.h"
#include "preparsed.h"
#include <cstdint>
#include <fstream>
#include <stdio.h>
#include <vector>

#define CHECK(variable, value)                                                 \
    {                                                                          \
        if (state.variable != value) {                                         \
            printf("Wrong %s: %d != %d", #variable, state.variable, value);    \
            return false;                                                      \
        }                                                                      \
    }
using namespace std;

void print_button(const char* msg, location_t location)
{
    if (location.exist) {
        printf("  %s: ", msg);
        printf("Relative: %d, size: %d, report: %d, start_address: %d\n",
            location.relative, 1 << location.size_, /*location.report*/ 0,
            location.start_address);
    }
}

int print(char* filename)
{
    ifstream file(filename, ios::binary | ios::ate);
    streamsize size = file.tellg();
    file.seekg(0, ios::beg);

    vector<char> buffer(size);
    if (file.read(buffer.data(), size)) {
        format_t format = { 0 };
        if (parse_format((uint8_t*)&buffer[0], size, &format)) {
            printf("Parse successful\n");
        }
        printf("Format: \n");
        uint8_t* buf = (uint8_t*)&format;
        for (int i = 0; i < sizeof(format); i++)
            printf("%02X ", buf[i]);
        printf("\n  hat angular: %d\n  use reports: %d\n", format.hat_angular,
            format.use_reports);
        print_button("Color left", format.colors[0]);
        print_button("Color down", format.colors[1]);
        print_button("Color right", format.colors[2]);
        print_button("Color up", format.colors[3]);
        print_button("Select", format.select);
        print_button("Start", format.start);
        print_button("Hat 1", format.joystick_press[0]);
        print_button("Hat 2", format.joystick_press[1]);
        print_button("L1", format.top_trigger[0]);
        print_button("R1", format.top_trigger[1]);
        print_button("Logo", format.logo);
        print_button("Hat", format.hat);
        print_button("Joystick 1X", format.joystick[0][0]);
        print_button("Joystick 1Y", format.joystick[0][1]);
        print_button("Joystick 2X", format.joystick[1][0]);
        print_button("Joystick 2Y", format.joystick[1][1]);
        print_button("Left trigger", format.triggers[0]);
        print_button("Right trigger", format.triggers[1]);
        return 0;
    } else {
        return 1;
    }
}

bool check_parse(char* filename, uint8_t* expected)
{
    ifstream file(filename, ios::binary | ios::ate);
    streamsize size = file.tellg();
    file.seekg(0, ios::beg);

    vector<char> buffer(size);
    if (file.read(buffer.data(), size)) {
        format_t format = { 0 };
        if (!parse_format((uint8_t*)&buffer[0], size, &format)) {
            printf("Could not parse\n");
            return false;
        }
        uint8_t* buf = (uint8_t*)&format;
        bool valid   = true;
        for (int i = 0; i < sizeof(format); i++) {
            if (buf[i] != expected[i]) {
                valid = false;
                break;
            }
        }
        if (!valid) {
            printf("Error for descriptor %s\nExpected ", filename);
            for (int i = 0; i < sizeof(format); i++)
                printf("%02X ", buf[i]);
            printf("\nBut got instead ");
            for (int i = 0; i < sizeof(format); i++)
                printf("%02X ", buf[i]);
            printf("\n");
            return false;
        }
        return true;
    } else {
        printf("Could not open file %s\n", filename);
        return false;
    }
}

bool check_PS3()
{
    format_t format = PS3_descr();
    // clang-format off
    uint8_t SAMPLE_REPORT[]       = {
        0x01, 0x00, 0x80, 0x8A, 0x00, 0x00, 0x7C, 0x82, // (for layout)
        0x84, 0x74, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //
        0x00, 0x00, 0x00, 0x48, 0x3E, 0x00, 0x00, 0x00, //
        0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xEE, 0x12, //
        0x00, 0x00, 0x00, 0x00, 0x12, 0xAD, 0x77, 0x00, //
        0x40, 0xFE, 0x01, 0xE7, 0x01, 0x90, 0x01, 0x02, //
        0x00,
    };
    // clang-format on
    CrcUtility::RemoteState state = { 0 };
    read_state(state, format, SAMPLE_REPORT, 49);
    CHECK(joystick1X, 0x7C);
    CHECK(joystick1Y, 0x82);
    CHECK(joystick2X, 0x84);
    CHECK(joystick2Y, 0x74);
    CHECK(arrowUp, false);
    CHECK(arrowLeft, true);
    CHECK(arrowDown, false);
    CHECK(arrowRight, false);
    CHECK(colorUp, false);
    CHECK(colorLeft, true);
    CHECK(colorDown, false);
    CHECK(colorRight, false);
    CHECK(select, false);
    CHECK(start, false);
    CHECK(logo, false);
    CHECK(hatL, false);
    CHECK(hatR, false);
    CHECK(L1, false);
    CHECK(R1, true);
    CHECK(gachetteG, 0x48);
    CHECK(gachetteD, 0x3E);
    return true;
}

bool check_LOGITECH()
{
    format_t format = F130_descr();
    uint8_t SAMPLE_REPORT[]
        = { 0x75, 0x7F, 0x80, 0x7F, 0x41, 0x16, 0x00, 0xFF };
    CrcUtility::RemoteState state = { 0 };
    read_state(state, format, SAMPLE_REPORT, 8);

    CHECK(joystick1X, 0x75);
    CHECK(joystick1Y, 0x7F);
    CHECK(joystick2X, 0x80);
    CHECK(joystick2Y, 0x7F);
    CHECK(arrowUp, true);
    CHECK(arrowLeft, false);
    CHECK(arrowDown, false);
    CHECK(arrowRight, true);
    CHECK(colorUp, false);
    CHECK(colorLeft, false);
    CHECK(colorDown, false);
    CHECK(colorRight, true);
    CHECK(select, true);
    CHECK(start, false);
    CHECK(logo, false);
    CHECK(hatL, false);
    CHECK(hatR, false);
    CHECK(L1, false);
    CHECK(R1, true);
    CHECK(gachetteG, 1);
    CHECK(gachetteD, 0);
    return true;
}

#ifdef MAIN_TEST
int main(int argc, char** argv)
{
    if (argc > 1) {
        for (int i = 1; i < argc; i++) {
            if (!print(argv[i])) {
                return 1;
            }
        }
        return 0;
    } else {
        // C++, so no designated initializer = tests would take SOOO much place
        // This will at least warn for regressions
        uint8_t DESC_1[] = { 0x81, 0x08, 0xA1, 0x08, 0xC1, 0x08, 0xE1, 0x08,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x09, 0x08, 0x0D, 0x00, 0x0D, 0x01, 0x0D, 0x02,
            0x0D, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
        uint8_t DESC_2[] = { 0x81, 0x00, 0xA1, 0x00, 0xC1, 0x00, 0xE1, 0x00,
            0x61, 0x01, 0x81, 0x01, 0xA1, 0x01, 0x00, 0x00, 0x01, 0x01, 0x21,
            0x01, 0x00, 0x00, 0x09, 0x00, 0x0D, 0x02, 0x0D, 0x03, 0x00, 0x00,
            0x00, 0x00, 0xC5, 0x01, 0x41, 0x01, 0x00, 0x01 };
        uint8_t DESC_3[] = { 0x81, 0x04, 0xA1, 0x04, 0xC1, 0x04, 0xE1, 0x04,
            0x81, 0x05, 0xA1, 0x05, 0xC1, 0x05, 0xE1, 0x05, 0x01, 0x05, 0x21,
            0x05, 0x00, 0x00, 0x09, 0x04, 0x0D, 0x00, 0x0D, 0x01, 0x0D, 0x02,
            0x0D, 0x03, 0x41, 0x05, 0x61, 0x05, 0x01, 0x00 };
        uint8_t DESC_4[] = { 0x81, 0x01, 0xA1, 0x01, 0xC1, 0x01, 0xE1, 0x01,
            0x81, 0x02, 0xA1, 0x02, 0xC1, 0x02, 0xE1, 0x02, 0x01, 0x02, 0x21,
            0x02, 0x00, 0x00, 0x09, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x41, 0x02, 0x61, 0x02, 0x00, 0x01 };
        if (!check_parse("example-descriptors/1.bin", DESC_1)) {
            return 1;
        }
        if (!check_parse("example-descriptors/2.bin", DESC_2)) {
            return 1;
        }
        if (!check_parse("example-descriptors/3.bin", DESC_3)) {
            return 1;
        }
        if (!check_parse("example-descriptors/4.bin", DESC_4)) {
            return 1;
        }
        if (!check_PS3()) {
            return 1;
        }
        if (!check_LOGITECH()) {
            return 1;
        }
        return 0;
    }
}
#endif
