#include "joy_state.h"

// Extract/implement a data field from/to a little endian report (bit array).
//
// Code sort-of follows HID spec:
//     http://www.usb.org/developers/hidpage/HID1_11.pdf
uint8_t read_value(location_t location, uint8_t* buf, uint16_t len)
{
    int8_t n = 1 << location.size_;
    if (!location.exist || len * 8 < n + location.start_address)
        return 0;

    uint8_t idx          = location.start_address / 8;
    uint8_t bit_shift    = location.start_address % 8;
    uint8_t bits_to_copy = 8 - bit_shift;
    uint32_t mask        = n < 32 ? (1U << n) - 1 : ~0U;

    uint32_t value = (uint32_t)buf[idx] >> bit_shift;
    n -= bits_to_copy;
    idx++;

    uint8_t bit_nr = bits_to_copy;
    while (n > 0) {
        value |= buf[idx] << bit_nr;
        n -= 8;
        bit_nr += 8;
        idx++;
    }

    return map(value & mask, 0, mask, 0, 255);
}

bool read_button_offset(
    location_t location, uint8_t* buf, uint16_t len, uint8_t offset)
{
    uint8_t address = location.start_address + offset;
    if (location.exist && len * 8 >= address + 1) {
        uint8_t shift = address % 8;

        // Not necessary, but it's clearer to make a comparison anyway
        return ((buf[address / 8] >> shift) & 1) != 0;
    } else {
        return 0;
    }
}

bool read_button(location_t location, uint8_t* buf, uint16_t len)
{
    return read_button_offset(location, buf, len, 0);
}

// Conversion for the hat
// 0000 => 0001
// 0001 => 0011
// 0010 => 0010
// 0011 => 0110
// 0100 => 0100
// 0101 => 1100
// 0110 => 1000
// 0111 => 1001
// 1000 => 0000
//
// It would be possible to make clever binary tricks, but unless it becomes
//   necessary, a simple switch is clearer.
void read_hat(bool* left,
    bool* bottom,
    bool* right,
    bool* up,
    location_t location,
    bool angular,
    uint8_t* buf,
    uint16_t len)
{
    if (angular) {
        uint8_t val = read_value(location, buf, len);
        switch (val) {
        case 0:
            *left   = false;
            *bottom = false;
            *right  = false;
            *up     = true;
            break;
        case 1:
            *left   = false;
            *bottom = false;
            *right  = true;
            *up     = true;
            break;
        case 2:
            *left   = false;
            *bottom = false;
            *right  = true;
            *up     = false;
            break;
        case 3:
            *left   = false;
            *bottom = true;
            *right  = true;
            *up     = false;
            break;
        case 4:
            *left   = false;
            *bottom = true;
            *right  = false;
            *up     = false;
            break;
        case 5:
            *left   = true;
            *bottom = true;
            *right  = false;
            *up     = false;
            break;
        case 6:
            *left   = true;
            *bottom = false;
            *right  = false;
            *up     = false;
            break;
        case 7:
            *left   = true;
            *bottom = false;
            *right  = false;
            *up     = true;
            break;
        default:
            *left   = false;
            *bottom = false;
            *right  = false;
            *up     = false;
            break;
        }
    } else {
        *up     = read_button_offset(location, buf, len, 0);
        *right  = read_button_offset(location, buf, len, 1);
        *bottom = read_button_offset(location, buf, len, 2);
        *left   = read_button_offset(location, buf, len, 3);
    }
}

void read_state(CrcUtility::RemoteState& state,
    format_t& format,
    uint8_t* buf,
    uint32_t len)
{
    if (format.use_reports) {
        buf += 1; // Skip report ID
        len -= 1;
    }

    read_hat(&state.arrowLeft, &state.arrowDown, &state.arrowRight,
        &state.arrowUp, format.hat, format.hat_angular, buf, len);
    state.colorLeft  = read_button(format.colors[0], buf, len);
    state.colorDown  = read_button(format.colors[1], buf, len);
    state.colorRight = read_button(format.colors[2], buf, len);
    state.colorUp    = read_button(format.colors[3], buf, len);
    state.L1         = read_button(format.top_trigger[0], buf, len);
    state.R1         = read_button(format.top_trigger[1], buf, len);
    state.select     = read_button(format.select, buf, len);
    state.start      = read_button(format.start, buf, len);
    state.logo       = read_button(format.logo, buf, len);
    state.hatL       = read_button(format.joystick_press[0], buf, len);
    state.hatR       = read_button(format.joystick_press[1], buf, len);
    state.joystick1X = read_value(format.joystick[0][0], buf, len);
    state.joystick1Y = read_value(format.joystick[0][1], buf, len);
    state.joystick2X = read_value(format.joystick[1][0], buf, len);
    state.joystick2Y = read_value(format.joystick[1][1], buf, len);
    state.gachetteG  = read_value(format.triggers[0], buf, len);
    state.gachetteD  = read_value(format.triggers[1], buf, len);
}
