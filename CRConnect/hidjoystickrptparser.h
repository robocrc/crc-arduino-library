#if !defined(__HIDJOYSTICKRPTPARSER_H__)
#define __HIDJOYSTICKRPTPARSER_H__

#include "hidparser.h"
#include <CrcLib.h>
#include <XBee.h>
#include <hid.h>
#include <hidcomposite.h>
#include <hidescriptorparser.h>
#include <hiduniversal.h>
#include <usbhub.h>

namespace impl {
const uint16_t LOGITECH_PID = 0xC216;
const uint16_t LOGITECH_VID = 0x046D;
const uint16_t HAVIT_PID    = 0x0001;
const uint16_t HAVIT_VID    = 0x0810;

class CrcHIDParser : public USBReadParser {
    bool valid = false;
    format_t& format;

public:
    CrcHIDParser(format_t& format);

    virtual void Parse(
        const uint32_t len, const uint8_t* pbuf, const uint32_t& offset);
    bool is_valid();
};

class JoystickReportParser : public HIDReportParser {
    bool _ready;
    format_t& format;
    CrcUtility::RemoteState _state;

public:
    JoystickReportParser(format_t& format);

    virtual void Parse(
        HID* hid, uint32_t is_rpt_id, uint32_t len, uint8_t* buf);

    void ready();

    CrcUtility::RemoteState State();
    bool isReady();
};
}

class HIDJoystick : public HIDUniversal {
    format_t format;
    impl::JoystickReportParser parser;

public:
    HIDJoystick(USBHost* usb)
        : HIDUniversal(usb)
        , format({ 0 })
        , parser(impl::JoystickReportParser(format)) {};

    CrcUtility::RemoteState State();
    uint32_t isReady();

protected:
    uint32_t OnInitSuccessful();
};

#endif // __HIDJOYSTICKRPTPARSER_H__
