#include "preparsed.h"

format_t PS3_descr()
{
    return (format_t){
        .hat_angular = false,
        .use_reports = true,
        .hat = { .exist = true, .size_ = 2, .start_address = 12, 0 },
        .select = { .exist = true, .start_address = 8, 0 },
        .joystick_press = {
            { .exist = true, .start_address = 9, 0 }, // left
            { .exist = true, .start_address = 10, 0 }, // right
        },
        .start = { .exist = true, .start_address = 11, 0 },
        .colors = {
            { .exist = true, .start_address = 23, 0}, // left
            { .exist = true, .start_address = 22, 0 }, // bottom
            { .exist = true, .start_address = 21, 0 }, // right
            { .exist = true, .start_address = 20, 0 }, // top
        },
        .top_trigger = {
            { .exist = true, .start_address = 18, 0 }, // left
            { .exist = true, .start_address = 19, 0 }, // right
        },
        .logo = { .exist = true, .start_address = 24, 0 },
        .joystick = {{ // left
            { .exist = true, .size_ = 3, .start_address = 40, 0 },
            { .exist = true, .size_ = 3, .start_address = 48, 0 },
        }, { // right
            { .exist = true, .size_ = 3, .start_address = 56, 0 },
            { .exist = true, .size_ = 3, .start_address = 64, 0 },
        }},
        .triggers = { // Technically this is not reported by the HID report, but hacks
            { .exist = true, .size_ = 3, .start_address = 144, 0 }, // left
            { .exist = true, .size_ = 3, .start_address = 152, 0 }, // right
    },
    };
}

format_t F130_descr()
{
    return (format_t){
        .hat_angular = true,
        .use_reports = false,
        .joystick = {{ // left
            { .exist = true, .size_ = 3, .start_address = 0, 0 },
            { .exist = true, .size_ = 3, .start_address = 8, 0 },
        }, { // right
            { .exist = true, .size_ = 3, .start_address = 16, 0 },
            { .exist = true, .size_ = 3, .start_address = 24, 0 },
        }},
        .hat = { .exist = true, .size_ = 2, .start_address = 32, 0 },
        .colors = {
            { .exist = true, .start_address = 36, 0}, // left
            { .exist = true, .start_address = 37, 0 }, // bottom
            { .exist = true, .start_address = 38, 0 }, // right
            { .exist = true, .start_address = 39, 0 }, // top
        },
        .top_trigger = {
            { .exist = true, .start_address = 40, 0 }, // left
            { .exist = true, .start_address = 41, 0 }, // right
        },
        .triggers = { // Technically this is not reported by the HID report, but hacks
            { .exist = true, .start_address = 42, 0 }, // left
            { .exist = true, .start_address = 43, 0 }, // right
		},
        .select = { .exist = true, .start_address = 44, 0 },
        .start = { .exist = true, .start_address = 45, 0 },
        .joystick_press = {
            { .exist = true, .start_address = 46, 0 }, // left
            { .exist = true, .start_address = 47, 0 }, // right
        },
        .logo = { .exist = false, 0 },
    };
}

format_t havit_descr()
{
    return (format_t){
        .hat_angular = true,
        .use_reports = true,
        .hat = { .exist = true, .size_ = 2, .start_address = 32, 0 },
        .select = { .exist = true, .start_address = 44, 0 },
        .joystick_press = {
            { .exist = true, .start_address = 46, 0 }, // left
            { .exist = true, .start_address = 47, 0 }, // right
        },
        .start = { .exist = true, .start_address = 45, 0 },
        .colors = {
            { .exist = true, .start_address = 39, 0}, // left
            { .exist = true, .start_address = 38, 0 }, // bottom
            { .exist = true, .start_address = 37, 0 }, // right
            { .exist = true, .start_address = 36, 0 }, // top
        },
        .top_trigger = {
            { .exist = true, .start_address = 40, 0 }, // left
            { .exist = true, .start_address = 41, 0 }, // right
        },
        .logo = { .exist = false, 0 },
        .joystick = {{ // left
            { .exist = true, .size_ = 3, .start_address = 16, 0 },
            { .exist = true, .size_ = 3, .start_address = 24, 0 },
        }, { // right
            { .exist = true, .size_ = 3, .start_address = 0, 0 },
            { .exist = true, .size_ = 3, .start_address = 8, 0 },
        }},
        .triggers = {
            { .exist = true, .start_address = 42, 0 }, // left
            { .exist = true, .start_address = 43, 0 }, // right
        },
    };
}
