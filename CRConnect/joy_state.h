#ifndef JOY_STATE_H
#define JOY_STATE_H
#ifdef MAIN_TEST
// Load directly if not using arduino
#include "../LibraryTestSketch/CrcLib/CrcRemoteState.h"
#else
#include <CrcLib.h>
#endif
#include "hidparser.h"
#include <stdbool.h>
#include <stdint.h>

// Read the state of the joystick according to the corresponding format
void read_state(CrcUtility::RemoteState& state,
    format_t& format,
    uint8_t* buf,
    uint32_t len);
#endif // JOY_STATE_H
