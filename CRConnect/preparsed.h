/* Preparsed HID descriptors. Avoid the processing overhead and makes sure the
 * descriptor is read as expected */

#ifndef PREPARSED_H
#define PREPARSED_H
#include "hidparser.h"

#ifdef __cplusplus
extern "C" {
#endif
format_t PS3_descr();

format_t F130_descr();

format_t havit_descr();
#ifdef __cplusplus
}
#endif
#endif // PREPARSED_H
