// DEBUG_USB_HOST => comment line 26 of USB_Host_Library_SAMD/src/Usb.h

#include <CrcLib.h>
#include <XBee.h>
#include <hid.h>
#include <hiduniversal.h>
#include <usbhub.h>
#include <PS3USB.h>
#include <PS4USB.h>
#include <XBOXONE.h>
#include <XBOXUSB.h>
#include <XBOXRECV.h>
#include "wiring_private.h"

#include "hidjoystickrptparser.h"

const int LED_COM_G = MOSI;
const int LED_COM_R = SCK;
const int BATT_READ = A0;
const int TEMP = A3;
const int XBEE_RESET = A4;
const int XBEE_13 = 8;
const int XBEE_TXD = 1;
const int XBEE_RXD = 0;
const int LED_REM_G = MISO;
const int LED_REM_R = 2;
const int LED_BATT_G = 5;
const int LED_BATT_R = 11;
const int SW_DETECT = 10;
const int BATT_STATE = 12;
const int ARD_TX = 6;
const int ARD_RX = 7;
const int USB_ON = SDA;
const int KEEP_ALIVE = SCL;

XBee xbee = XBee();

// Addresse du XBee remote (initialisé dans setup)
XBeeAddress64 addr64;

USBHost UsbH;
PS3USB PS3(&UsbH);
XBOXRECV Xbox360r(&UsbH);
XBOXUSB Xbox360(&UsbH);
XBOXONE XboxOne(&UsbH);
HIDJoystick Hid(&UsbH);
PS4USB PS4(&UsbH);

Uart ardSerial(&sercom3, ARD_RX, ARD_TX, SERCOM_RX_PAD_3, UART_TX_PAD_2);

void SERCOM3_Handler() {
  ardSerial.IrqHandler();
}

// Wrapper pour faire une requête au xbee
// Si ok n'est pas true, la réponse est invalide et ne devrait pas être lue
AtCommandResponse request(XBee xbee, char* cmd, uint8_t* data, uint8_t len, bool* ok)
{
  AtCommandRequest atRequest = AtCommandRequest((uint8_t*)cmd, data, len);
  AtCommandResponse atResponse;
  xbee.send(atRequest);

  // after sending a tx request, we expect a status response
  // wait up to half second for the status response
  if (xbee.readPacket(5000)) {
    // Serial.println("got a response!");
    XBeeResponse& resp = xbee.getResponse();

    // should be an AT command response
    if (resp.getApiId() == AT_COMMAND_RESPONSE) {
      resp.getAtCommandResponse(atResponse);

      if (atResponse.isOk()) {
        *ok = true;
        return atResponse;
      } else {
        // Serial.print("Command return error code: ");
        // Serial.println(atResponse.getStatus(), HEX);
      }
    } else {
      // Serial.print("Expected AT response but got ");
      // Serial.print(xbee.getResponse().getApiId(), HEX);
    }
  } else if (xbee.getResponse().isError()) {
    // Serial.print("Error reading packet.  Error code: ");
    // Serial.println(xbee.getResponse().getErrorCode());
  } else {
    // Serial.println("local XBee did not provide a timely TX Status
    // Response -- should not happen");
  }

  *ok = false;
  return atResponse;
}

// Wrapper pour faire une requête au xbee distant
// Si ok n'est pas true, la réponse est invalide et ne devrait pas être lue
AtCommandResponse remoteRequest(XBee xbee, char* cmd, uint8_t* data, uint8_t len, bool* ok, XBeeAddress64 addr)
{
  RemoteAtCommandRequest atRequest = RemoteAtCommandRequest(addr, (uint8_t*)cmd, data, len);
  RemoteAtCommandResponse atResponse;
  xbee.send(atRequest);

  // after sending a tx request, we expect a status response
  // wait up to half second for the status response
  if (xbee.readPacket(5000)) {
    // Serial.println("got a response!");
    XBeeResponse& resp = xbee.getResponse();

    // should be an AT command response
    if (resp.getApiId() == RemoteAtCommandResponse::API_ID) {
      resp.getRemoteAtCommandResponse(atResponse);

      if (atResponse.isOk()) {
        *ok = true;
        return atResponse;
      } else {
        // Serial.print("Command return error code: ");
        // Serial.println(atResponse.getStatus(), HEX);
      }
    } else {
      // Serial.print("Expected AT response but got ");
      // Serial.print(xbee.getResponse().getApiId(), HEX);
    }
  } else if (xbee.getResponse().isError()) {
    // Serial.print("Error reading packet.  Error code: ");
    // Serial.println(xbee.getResponse().getErrorCode());
  } else {
    // Serial.println("local XBee did not provide a timely TX Status
    // Response -- should not happen");
  }

  *ok = false;
  return atResponse;
}

// Calcule l'address du XBee remote à partir de commandes AT
bool getAddressAndInitialize(XBee xbee, XBeeAddress64& addr)
{
  bool ok;
  AtCommandResponse atResponse = request(xbee, "DH", NULL, 0, &ok);
  if (!ok)
    return false;
  // Serial.println("Success");
  if (atResponse.getValueLength() != 4) {
    // Serial.println("Wrong length?");
    return false;
  }

  uint32_t high = ((uint32_t)atResponse.getValue()[0] << 24)
                  | ((uint32_t)atResponse.getValue()[1] << 16)
                  | ((uint32_t)atResponse.getValue()[2] << 8)
                  | ((uint32_t)atResponse.getValue()[3]);

  atResponse = request(xbee, "DL", NULL, 0, &ok);
  if (!ok)
    return false;
  // Serial.println("Success");
  if (atResponse.getValueLength() != 4) {
    // Serial.println("Wrong length?");
    return false;
  }

  uint32_t low = ((uint32_t)atResponse.getValue()[0] << 24)
                 | ((uint32_t)atResponse.getValue()[1] << 16)
                 | ((uint32_t)atResponse.getValue()[2] << 8)
                 | ((uint32_t)atResponse.getValue()[3]);

  // Serial.print(high, HEX);
  // Serial.print(" ");
  // Serial.println(low, HEX);
  addr = XBeeAddress64(high, low);

  uint8_t data[] = {};
  remoteRequest(xbee, "FR", data, 0, &ok, addr); // Ignore reset result
  return true;
}

CrcUtility::RemoteState PS4_state() {
  CrcUtility::RemoteState state = { 0 };
  state.joystick1X = PS4.getAnalogHat(LeftHatX);
  state.joystick1Y = PS4.getAnalogHat(LeftHatY);
  state.joystick2X = PS4.getAnalogHat(RightHatX);
  state.joystick2Y = PS4.getAnalogHat(RightHatY);
  state.gachetteG = PS4.getAnalogButton(L2);
  state.gachetteD = PS4.getAnalogButton(R2);
  state.logo = PS4.getButtonPress(PS);
  state.colorLeft = PS4.getButtonPress(SQUARE);
  state.colorDown = PS4.getButtonPress(CROSS);
  state.colorRight = PS4.getButtonPress(CIRCLE);
  state.colorUp = PS4.getButtonPress(TRIANGLE);
  state.arrowLeft = PS4.getButtonPress(LEFT);
  state.arrowDown = PS4.getButtonPress(DOWN);
  state.arrowRight = PS4.getButtonPress(RIGHT);
  state.arrowUp = PS4.getButtonPress(UP);
  state.L1 = PS4.getButtonPress(L1);
  state.R1 = PS4.getButtonPress(R1);
  state.hatL = PS4.getButtonPress(L3);
  state.hatR = PS4.getButtonPress(R3);
  state.select = PS4.getButtonPress(SELECT);
  state.start = PS4.getButtonPress(START);

  return state;
}

CrcUtility::RemoteState PS3_state() {
  CrcUtility::RemoteState state = { 0 };
  state.joystick1X = PS3.getAnalogHat(LeftHatX);
  state.joystick1Y = PS3.getAnalogHat(LeftHatY);
  if (PS3.PS3Connected) { // The Navigation controller only have one joystick
    state.joystick2X = PS3.getAnalogHat(RightHatX);
    state.joystick2Y = PS3.getAnalogHat(RightHatY);
  }
  state.gachetteG = PS3.getAnalogButton(L2);
  if (PS3.PS3Connected) { // Only left side
    state.gachetteD = PS3.getAnalogButton(R2);
  }
  state.logo = PS3.getButtonPress(PS);
  state.colorLeft = PS3.getButtonPress(SQUARE);
  state.colorDown = PS3.getButtonPress(CROSS);
  state.colorRight = PS3.getButtonPress(CIRCLE);
  state.colorUp = PS3.getButtonPress(TRIANGLE);
  state.arrowLeft = PS3.getButtonPress(LEFT);
  state.arrowDown = PS3.getButtonPress(DOWN);
  state.arrowRight = PS3.getButtonPress(RIGHT);
  state.arrowUp = PS3.getButtonPress(UP);
  state.L1 = PS3.getButtonPress(L1);
  state.R1 = PS3.getButtonPress(R1);
  state.hatL = PS3.getButtonPress(L3);
  state.hatR = PS3.getButtonPress(R3);
  state.select = PS3.getButtonPress(SELECT);
  state.start = PS3.getButtonPress(START);

  return state;
}

CrcUtility::RemoteState XboxOne_state() {
  CrcUtility::RemoteState state = { 0 };
  state.joystick1X = (XboxOne.getAnalogHat(LeftHatX) >> 8) + 128;
  state.joystick1Y = (XboxOne.getAnalogHat(LeftHatY) >> 8) + 128;
  state.joystick2X = (XboxOne.getAnalogHat(RightHatX) >> 8) + 128;
  state.joystick2Y = (XboxOne.getAnalogHat(RightHatY) >> 8) + 128;
  state.gachetteG = XboxOne.getButtonPress(L2) / 4;
  state.gachetteD = XboxOne.getButtonPress(R2) / 4;
  state.logo = XboxOne.getButtonPress(XBOX);
  state.colorLeft = XboxOne.getButtonPress(X);
  state.colorDown = XboxOne.getButtonPress(A);
  state.colorRight = XboxOne.getButtonPress(B);
  state.colorUp = XboxOne.getButtonPress(Y);
  state.arrowLeft = XboxOne.getButtonPress(LEFT);
  state.arrowDown = XboxOne.getButtonPress(DOWN);
  state.arrowRight = XboxOne.getButtonPress(RIGHT);
  state.arrowUp = XboxOne.getButtonPress(UP);
  state.L1 = XboxOne.getButtonPress(L1);
  state.R1 = XboxOne.getButtonPress(R1);
  state.hatL = XboxOne.getButtonPress(L3);
  state.hatR = XboxOne.getButtonPress(R3);
  state.select = XboxOne.getButtonPress(BACK);
  state.start = XboxOne.getButtonPress(START);

  return state;
}

CrcUtility::RemoteState Xbox360_state() {
  CrcUtility::RemoteState state = { 0 };
  state.joystick1X = (Xbox360.getAnalogHat(LeftHatX) >> 8) + 128;
  state.joystick1Y = 127 - (Xbox360.getAnalogHat(LeftHatY) >> 8);
  state.joystick2X = (Xbox360.getAnalogHat(RightHatX) >> 8) + 128;
  state.joystick2Y = 127 - (Xbox360.getAnalogHat(RightHatY) >> 8);
  state.gachetteG = Xbox360.getButtonPress(L2);
  state.gachetteD = Xbox360.getButtonPress(R2);
  state.logo = Xbox360.getButtonPress(XBOX);
  state.colorLeft = Xbox360.getButtonPress(X);
  state.colorDown = Xbox360.getButtonPress(A);
  state.colorRight = Xbox360.getButtonPress(B);
  state.colorUp = Xbox360.getButtonPress(Y);
  state.arrowLeft = Xbox360.getButtonPress(LEFT);
  state.arrowDown = Xbox360.getButtonPress(DOWN);
  state.arrowRight = Xbox360.getButtonPress(RIGHT);
  state.arrowUp = Xbox360.getButtonPress(UP);
  state.L1 = Xbox360.getButtonPress(L1);
  state.R1 = Xbox360.getButtonPress(R1);
  state.hatL = Xbox360.getButtonPress(L3);
  state.hatR = Xbox360.getButtonPress(R3);
  state.select = Xbox360.getButtonPress(SELECT);
  state.start = Xbox360.getButtonPress(START);

  return state;
}

CrcUtility::RemoteState Xbox360r_state() {
  CrcUtility::RemoteState state = { 0 };
  state.joystick1X = (Xbox360r.getAnalogHat(LeftHatX) >> 8) + 128;
  state.joystick1Y = 127 - (Xbox360r.getAnalogHat(LeftHatY) >> 8);
  state.joystick2X = (Xbox360r.getAnalogHat(RightHatX) >> 8) + 128;
  state.joystick2Y = 127 - (Xbox360r.getAnalogHat(RightHatY) >> 8);
  state.gachetteG = 255 * Xbox360r.getButtonPress(L2);
  state.gachetteD = 255 * Xbox360r.getButtonPress(R2);
  state.logo = Xbox360r.getButtonPress(XBOX);
  state.colorLeft = Xbox360r.getButtonPress(X);
  state.colorDown = Xbox360r.getButtonPress(A);
  state.colorRight = Xbox360r.getButtonPress(B);
  state.colorUp = Xbox360r.getButtonPress(Y);
  state.arrowLeft = Xbox360r.getButtonPress(LEFT);
  state.arrowDown = Xbox360r.getButtonPress(DOWN);
  state.arrowRight = Xbox360r.getButtonPress(RIGHT);
  state.arrowUp = Xbox360r.getButtonPress(UP);
  state.L1 = Xbox360r.getButtonPress(L1);
  state.R1 = Xbox360r.getButtonPress(R1);
  state.hatL = Xbox360r.getButtonPress(L3);
  state.hatR = Xbox360r.getButtonPress(R3);
  state.select = Xbox360r.getButtonPress(SELECT);
  state.start = Xbox360r.getButtonPress(START);

  return state;
}

uint16_t adc_read(int port, uint16_t* prev, uint8_t max_vals, uint8_t* vals, bool adc, bool adc_preload) {
  uint32_t sum = 0;
  if (adc_preload) {
    analogRead(port); // Make a measurement to set the ADC to the correct level before making a valid reading
    delay(5);
  }
  if (adc) { // If adc, shift previous values and make a new reading
    *vals = min((*vals) + 1, max_vals);
    for (uint8_t i = (*vals) - 1; i > 0; i--) {
      prev[i] = prev[i - 1];
      sum += prev[i];
    }
    prev[0] = analogRead(port);
    sum += prev[0];
  } else {
    for (uint8_t i = 0; i < (*vals); i++) {
      sum += prev[i];
    }
  }
  return (uint16_t)(sum / (*vals));
}

void check_temp(bool clk, bool adc, bool adc_preload) {
  const uint8_t NUM_VALS = 4;
  static uint16_t prev[NUM_VALS];
  static uint8_t vals = 0;

  // TEMP => 10 mV/°C + 500mV
  // 3.3V = 280°C
  // 1.65V = 115°C
  // 0V = -50°C
  // LiPos tend to explode around 60°C, so shutdown at ~50°C
  int16_t mapped_temp = map(adc_read(TEMP, prev, NUM_VALS, &vals, adc, adc_preload), 0, 4095, -500, 2800); // tenth of degrees
  ardSerial.println("Vals: ");
  for (uint8_t i = 0; i < vals; i++) {
    ardSerial.println(prev[i]);
  }
  ardSerial.print("Mapped: ");
  ardSerial.println(mapped_temp);
  ardSerial.println("========");
  if (vals < 4) return; // Don't quit on first four values
  /* Remove '*' to comment -> */
  if (mapped_temp >= 50 * 10) {
    for (uint8_t i = 0; i < 10; i++) { // 5 times
      bool on = i % 2 == 0;
      digitalWrite(LED_REM_G, on);
      digitalWrite(LED_REM_R, on);
      digitalWrite(LED_BATT_G, on);
      digitalWrite(LED_BATT_R, on);
      digitalWrite(LED_COM_G, on);
      digitalWrite(LED_COM_R, on);
      delay(500);
    }
    // Shutdown
    digitalWrite(KEEP_ALIVE, LOW);
    while (true); // Stop board

  } else /* End */if (mapped_temp >= 40 * 10) { // Getting high
    digitalWrite(LED_REM_G, HIGH);
    digitalWrite(LED_REM_R, clk);
  }
}

void check_batt(bool clk, bool adc, bool adc_preload) {
  const uint8_t NUM_VALS = 4;
  static uint16_t prev[NUM_VALS];
  static uint8_t vals = 0;
  static bool state = false;

  uint16_t batt_mapped = map(adc_read(BATT_READ, prev, NUM_VALS, &vals, adc, adc_preload), 0, 4095, 0, 6600); // hundredth of volts + voltage divider
  if (batt_mapped < 3500 && !digitalRead(BATT_STATE) && vals >= 2) { // Kill (Don't quit on first two values)
    digitalWrite(KEEP_ALIVE, LOW);
    while (true); // Stop board
  } else if (batt_mapped < 3600) {
    digitalWrite(LED_REM_G, LOW);
    digitalWrite(LED_REM_R, clk);
  } else if (batt_mapped < 3700) {
    digitalWrite(LED_REM_G, LOW);
    digitalWrite(LED_REM_R, HIGH);
  } else if (batt_mapped < 3800) {
    digitalWrite(LED_REM_G, HIGH);
    digitalWrite(LED_REM_R, HIGH);
  } else {
    digitalWrite(LED_REM_G, HIGH);
    digitalWrite(LED_REM_R, LOW);
  }
}

void check_conn(bool clk, bool valid, bool sending, uint8_t lvl) {
  if (!sending) {
    digitalWrite(LED_COM_G, LOW);
    digitalWrite(LED_COM_R, LOW);
  } else if (!valid) {
    digitalWrite(LED_COM_R, clk);
    digitalWrite(LED_COM_G, false);
  } else if (lvl == 0) {
    digitalWrite(LED_COM_G, HIGH);
    digitalWrite(LED_COM_R, LOW);
  } else if (lvl == 1) {
    digitalWrite(LED_COM_G, HIGH);
    digitalWrite(LED_COM_R, HIGH);
  } else if (lvl == 2) {
    digitalWrite(LED_COM_G, LOW);
    digitalWrite(LED_COM_R, HIGH);
  }
}

void check_CRCduino(bool clk_state, uint8_t state) {
  state = (state >> (clk_state * 2)) & 0b11;
  digitalWrite(LED_BATT_G, (state & 0b10) != 0);
  digitalWrite(LED_BATT_R, (state & 0b01) != 0);
}

enum State {
  Waiting,
  Resetting,
  Resetting2,
  Gathering,
  Transmitting,
};
void upload(bool is_uno, char first_char) {
  const uint32_t DELAY_RESET = 800; // 0.8s
  const uint16_t BUFFER_LEN = 100;
  const uint8_t NUM_BUF = 2;
  uint8_t RESET_DIG[] = "D1";

  bool ok = false;
  uint8_t mode[] = { 0x00 };
  while (!ok) {
    remoteRequest(xbee, "AP", mode, sizeof(mode), &ok, addr64);
  }
  uint8_t speed[] = { 0x07 };
  ok = false;
  while (!ok) {
    remoteRequest(xbee, "BD", speed, sizeof(speed), &ok, addr64);
  }
  uint8_t retries[] = { 0x05 };
  ok = false;
  while (!ok) {
    remoteRequest(xbee, "RR", retries, sizeof(retries), &ok, addr64);
  }
  ok = false;
  while (!ok) {
    request(xbee, "RR", retries, sizeof(retries), &ok);
  }
  ok = false;
  while (!ok) {
    remoteRequest(xbee, "AC", NULL, 0, &ok, addr64);
  }
  uint8_t buffer[BUFFER_LEN][NUM_BUF] = {{ first_char }, {}};
  uint16_t buf_len[NUM_BUF] = {1, 0};
  uint8_t read_buf = 0;
  uint8_t send_buf = 0;
  State state = State::Waiting;
  uint32_t last_send = millis();
  uint32_t last_recv = millis();
  uint32_t reset;
  bool packet_ready = false;
  bool is_error = false;
  while (true) {
    while (Serial.available() > 0 && buf_len[read_buf] < BUFFER_LEN) {
      buffer[read_buf][buf_len[read_buf]] = Serial.read();
      buf_len[read_buf]++;
      last_recv = millis();
    }
    if (!packet_ready) {
      xbee.readPacket();
      packet_ready = xbee.getResponse().isAvailable() && (
                       xbee.getResponse().getApiId() == RemoteAtCommandResponse::API_ID
                       || xbee.getResponse().getApiId() == ZBTxStatusResponse::API_ID
                       || xbee.getResponse().getApiId() == ZBRxResponse::API_ID
                     );
      is_error = xbee.getResponse().isError();
    }
    digitalWrite(LED_BATT_R, Serial.available() > 0 && buf_len[read_buf] < BUFFER_LEN);
    digitalWrite(LED_BATT_G, is_error);
    digitalWrite(LED_REM_G, state == State::Waiting);
    digitalWrite(LED_REM_R, state == State::Transmitting);
    digitalWrite(LED_COM_G, read_buf == 0);
    digitalWrite(LED_COM_R, send_buf == 1);
    if (packet_ready && xbee.getResponse().getApiId() == ZBRxResponse::API_ID) {
      ZBRxResponse rx = ZBRxResponse();
      xbee.getResponse().getZBRxResponse(rx);
      for (uint16_t i = 0; i < rx.getDataLength(); i++) {
        Serial.write(rx.getData()[i]);
      }
      packet_ready = false;
    }
    if (state == State::Waiting && buf_len[read_buf] > 0) {
      uint8_t data[] = { 0x04 };
      RemoteAtCommandRequest atRequest = RemoteAtCommandRequest(addr64, RESET_DIG, data, sizeof(data));
      xbee.send(atRequest);
      state = State::Resetting;
    } else if (state == State::Resetting && packet_ready) {
      if (xbee.getResponse().getApiId() != RemoteAtCommandResponse::API_ID) {
        packet_ready = false;
        continue;
      }
      RemoteAtCommandResponse atResponse = RemoteAtCommandResponse();
      xbee.getResponse().getAtCommandResponse(atResponse);
      packet_ready = false;

      if (!atResponse.isOk() || strncmp((const char *)atResponse.getCommand(), (const char *)RESET_DIG, 2) != 0) {
        state = State::Waiting;
        continue;
      }
      uint8_t data[] = { 0x00 };
      RemoteAtCommandRequest atRequest = RemoteAtCommandRequest(addr64, RESET_DIG, data, sizeof(data));
      xbee.send(atRequest);
      state = State::Resetting2;
    } else if (state == State::Resetting2 && packet_ready) {
      if (xbee.getResponse().getApiId() != RemoteAtCommandResponse::API_ID) {
        packet_ready = false;
        continue;
      }
      RemoteAtCommandResponse atResponse = RemoteAtCommandResponse();
      xbee.getResponse().getAtCommandResponse(atResponse);
      packet_ready = false;

      if (!atResponse.isOk() || strncmp((const char *)atResponse.getCommand(), (const char *)RESET_DIG, 2) != 0) {
        state = State::Waiting;
        continue;
      }
      reset = millis();
      state = State::Gathering;
    } else if (millis() - reset < 150) { // wait for 150ms to allow reboot
    } else if (state == State::Gathering) {
      if (buf_len[send_buf] == 0 && (millis() - last_send) > DELAY_RESET) {
        state = State::Waiting;
        continue;
      }
      if (buf_len[send_buf] == 0) {
        continue;
      }
      if ((millis() - last_recv <= 1) && buf_len[send_buf] < BUFFER_LEN) {
        continue;
      }
      ZBTxRequest zbTx = ZBTxRequest(addr64, buffer[send_buf], buf_len[send_buf]);
      xbee.send(zbTx);

      if (read_buf == send_buf) {
        read_buf = (read_buf + 1) % NUM_BUF;
      }

      last_send = millis();
      state = State::Transmitting;
    } else if (state == State::Transmitting && packet_ready) {
      if (xbee.getResponse().getApiId() != ZBTxStatusResponse::API_ID) {
        packet_ready = false;
        continue;
      }
      ZBTxStatusResponse txStatus = ZBTxStatusResponse();
      xbee.getResponse().getZBTxStatusResponse(txStatus);
      packet_ready = false;

      if (txStatus.getDeliveryStatus() != SUCCESS) {
        state = State::Gathering;
        continue;
      }

      buf_len[send_buf] = 0;
      send_buf = (send_buf + 1) % NUM_BUF;
      state = State::Gathering;
    } else if (state == State::Transmitting && (is_error || (millis() - last_send) > 1000)) {
      packet_ready = false;
      state = State::Gathering;
    }
  }
}

void firmware() {
  Serial.begin(115200);
  while (true) {
    while (Serial.available() == 0);
    char c = Serial.read();
    switch (c) {
      case 0x7E: // XBee
      case 0x2B:
        Serial1.write(c);
        while (true) {
          if (Serial1.available() > 0 && Serial.availableForWrite() > 0) {
            Serial.write(Serial1.read());
          }
          if (Serial.available() > 0 && Serial1.availableForWrite() > 0) {
            Serial1.write(Serial.read());
          }
        }
        break;
      case 0x1B: // Upload to mega
        upload(false, 0x1B);
        break;
      case 0xFA: // Remote control
        while (true) {
          uint8_t buf[255] = { 0 };
          uint8_t len = 0;
          uint8_t next_packet = 0;
          if (Serial.available() > 0) {
            if (next_packet == 0) {
              next_packet = Serial.read();
            } else {
              buf[len] = Serial.read();
              len += 1;
            }
          }
          if (len == next_packet) {
            ZBTxRequest zbTx = ZBTxRequest(addr64, buf, len);
            xbee.send(zbTx);
            len = 0;
            next_packet = 0;
          }
          xbee.readPacket();
          if (xbee.getResponse().isAvailable() && xbee.getResponse().getApiId() == ZBRxResponse::API_ID) {
            ZBRxResponse rx = ZBRxResponse();
            xbee.getResponse().getZBRxResponse(rx);
            for (uint8_t i = 0; i < rx.getDataLength(); i++) {
              Serial.write(rx.getData()[i]);
            }
          }
        }
        break;
      default: // Unknow packet
        continue;
    }
  }
}

typedef enum {
  WAITING,
  SENT,
  ACKED,
  ERRORED,
} tx_state_t;

void setup()
{
  // Keep alive
  pinMode(KEEP_ALIVE, OUTPUT);
  digitalWrite(KEEP_ALIVE, HIGH);

  // Increase ADC resolution
  analogReadResolution(12);

  // Setup pins
  // LEDs
  pinMode(LED_REM_G, OUTPUT);
  pinMode(LED_REM_R, OUTPUT);
  pinMode(LED_BATT_G, OUTPUT);
  pinMode(LED_BATT_R, OUTPUT);
  pinMode(LED_COM_G, OUTPUT);
  pinMode(LED_COM_R, OUTPUT);
  // INPUTS
  pinMode(SW_DETECT, INPUT);
  pinMode(BATT_STATE, INPUT);
  // BATT_READ and TEMP are analog inputs

  attachInterrupt(digitalPinToInterrupt(SW_DETECT), self_shutdown, RISING);

  // This is an output on the XBee, DON'T push current
  pinMode(XBEE_13, INPUT);

  // Send power to USB
  pinMode(USB_ON, OUTPUT);
  digitalWrite(USB_ON, HIGH);

  // Setup uart ports
  ardSerial.begin(115200);
  pinPeripheral(ARD_RX, PIO_SERCOM_ALT);
  pinPeripheral(ARD_TX, PIO_SERCOM_ALT);

  digitalWrite(LED_REM_G, HIGH);
  digitalWrite(LED_REM_R, HIGH);
  digitalWrite(LED_BATT_G, HIGH);
  digitalWrite(LED_BATT_R, HIGH);
  digitalWrite(LED_COM_G, HIGH);
  digitalWrite(LED_COM_R, HIGH);

  unsigned long start = millis();
  Serial1.begin(115200);
  xbee.setSerial(Serial1);

  // Startup delay to wait for thermometer to initialize (theorically 800us, but well).
  while (millis() - start < 100 || digitalRead(SW_DETECT));
  bool firmware_mode = millis() - start > 1000; // Kept pressed for 1s
  check_temp(true, false, true);
  delay(100);
  pinMode(XBEE_RESET, OUTPUT);
  digitalWrite(XBEE_RESET, HIGH);
  delay(100);
  digitalWrite(XBEE_RESET, LOW);
  // Discard extra input
  while (Serial1.available()) {
    uint8_t _discard = Serial1.read();
  }
  // Get the address configured in the local XBee
  while (!getAddressAndInitialize(xbee, addr64)) {
    check_temp(true, true, false);
    delay(10);
  }
  if (firmware_mode) {
    digitalWrite(LED_REM_G, LOW);
    digitalWrite(LED_REM_R, HIGH);
    digitalWrite(LED_BATT_G, LOW);
    digitalWrite(LED_BATT_R, HIGH);
    digitalWrite(LED_COM_G, LOW);
    digitalWrite(LED_COM_R, HIGH);
    firmware();
  }

  if (UsbH.Init() != 0) {
    digitalWrite(KEEP_ALIVE, LOW); // TODO: shutdown properly
  }
  digitalWrite(LED_REM_G, LOW);
  digitalWrite(LED_REM_R, LOW);
  digitalWrite(LED_BATT_G, LOW);
  digitalWrite(LED_BATT_R, LOW);
  digitalWrite(LED_COM_G, LOW);
  digitalWrite(LED_COM_R, LOW);
}

void self_shutdown() {
  digitalWrite(KEEP_ALIVE, LOW);
}

// Light up leds depending on status
void leds(bool clear_to_adc, unsigned long now, unsigned long last_tx_ack, bool connected, uint8_t reception_lvl, bool sending, uint8_t batt_state) {
  static bool clk_state                     = true;
  static unsigned long clk_last_sw          = now;
  static uint8_t adc_state                  = 0;

  bool changed = false;
  if (now - clk_last_sw > 500 && clear_to_adc) {
    clk_last_sw = now;
    clk_state = !clk_state;
    adc_state = (adc_state + 1) % 10;
    changed = true;
  }

  // Shutdown and/or set leds based on battery. Before temp because
  // check_temp can override the values
  check_batt(clk_state, adc_state == 1 && changed, adc_state == 0 && changed);
  // Shutdown and/or set leds if temp too high
  check_temp(clk_state, adc_state > 2 && changed, adc_state == 2 && changed);
  // Set leds based on connection status
  check_conn(clk_state, connected, sending, reception_lvl);
  // Set leds based on CRCduino battery status
  check_CRCduino(clk_state, batt_state);
}

void loop()
{
  static unsigned long last_update          = millis();
  static unsigned long last_tx_ack          = millis();
  static tx_state_t tx_status               = WAITING;
  static CrcUtility::RemoteState tx_state   = { 0 }; // Last sent state
  static CrcUtility::RemoteState prev_state = { 0 }; // Last acked state
  static uint8_t reception_lvl              = 0;
  static unsigned long last_update_check    = millis();
  static uint8_t batt_state                 = 0;
  static bool sw_debounce                   = true;

  unsigned long now = millis();
  bool acked = (now - last_tx_ack) < 1000;
  if (!acked) {
    batt_state = 0;
    tx_status = ERRORED;  // Retry
  }
  leds(now - last_update >= 25, now, last_tx_ack, tx_status != ERRORED, reception_lvl, acked, batt_state);

  UsbH.Task();
  xbee.readPacket();
  if (xbee.getResponse().isAvailable()) {
    switch (xbee.getResponse().getApiId()) {
      case ZBTxStatusResponse::API_ID:
        {
          // should be a znet tx status
          ZBTxStatusResponse txStatus = ZBTxStatusResponse();
          xbee.getResponse().getZBTxStatusResponse(txStatus);

          // get the delivery status, the fifth byte
          if (txStatus.getDeliveryStatus() == SUCCESS) {
            // Serial.println("success.  time to celebrate");
            prev_state  = tx_state;
            last_tx_ack = now;
            tx_status = ACKED;
          } else {
            // Serial.println("the remote XBee did not receive our
            // packet. is it powered on?");
            tx_status  = ERRORED;
          }
          break;
        }
      case ZBRxResponse::API_ID:
        {
          ZBRxResponse rx = ZBRxResponse();
          xbee.getResponse().getZBRxResponse(rx);
          if (rx.getDataLength() == 1) {
            batt_state = rx.getData()[0];
          }
          break;
        }

      case ModemStatusResponse::API_ID:
        {
          break;
        }

      case AtCommandResponse::API_ID:
        {
          AtCommandResponse atResponse = AtCommandResponse();
          xbee.getResponse().getAtCommandResponse(atResponse);

          if (!atResponse.isOk() || atResponse.getValueLength() != 1) {
            tx_status = ERRORED;
            break;
          }
          uint8_t reception = atResponse.getValue()[0]; // Between 0x24 and 0x64
          if (reception <= 0x34) {
            reception_lvl = 0;
          } else if (reception <= 0x54) {
            reception_lvl = 1;
          } else {
            reception_lvl = 2;
          }
          break;
        }
      default:
        break;
    }
  } else if (xbee.getResponse().isError()) {
    tx_status = ERRORED;
    // Serial.print("Error reading packet.  Error code: ");
    // Serial.println(xbee.getResponse().getErrorCode());
  }

  if (now - last_update_check >= 5000) { // Passed next update time
    last_update_check = now;
    AtCommandRequest atRequest = AtCommandRequest((uint8_t*)"DB");
    xbee.send(atRequest);
  }
  bool broadcast = addr64 == RemoteAtCommandRequest::broadcastAddress64;
  if (now - last_update >= 50 && tx_status != SENT && Serial1.availableForWrite() > 21) {
    last_update = now;
    CrcUtility::RemoteState s = { 0 };
    bool connected_usb = false;
    if (Hid.isReady()) {
      s = Hid.State();
      connected_usb = true;
    } else if (PS3.PS3Connected || PS3.PS3NavigationConnected) {
      s = PS3_state();
      PS3.setLedOn(LED1); // Directly communicate
      connected_usb = true;
    } else if (PS4.connected()) {
      s = PS4_state();
      connected_usb = true;
    } else if (XboxOne.XboxOneConnected) {
      s = XboxOne_state();
      connected_usb = true;
    } else if (Xbox360.Xbox360Connected) {
      s = Xbox360_state();
      connected_usb = true;
    } else if (Xbox360r.XboxReceiverConnected) {
      s = Xbox360r_state();
      connected_usb = true;
    }
    if (connected_usb) {
      uint8_t send_buf[9]       = { 0 };
      uint8_t send_len = s.serialize_update(send_buf, true, prev_state);
      tx_state = s;
      // Only wait for ack in peer to peer
      tx_status = (broadcast) ? ACKED : SENT;
      ZBTxRequest zbTx = ZBTxRequest(addr64, send_buf, send_len);
      xbee.send(zbTx);
    }
  }
}
