#ifndef HIDPARSER_H
#define HIDPARSER_H
#ifdef __cplusplus
extern "C" {
#endif
#include <stdbool.h>
#include <stdint.h>

// Definition of the location of a given control. Fits in 2 bytes as to pack it
// without padding
//   in the format definition
typedef struct {
    bool exist : 1; // Does the field exist?
    bool relative : 1; // Is it an increment or an absolute value? Only for
                       // analogs
    uint8_t size_ : 3; // joystick resolution of 1 << field.
    /*uint8_t report : 3; // Number of the (potential) report of the field. 0 =
       no
                        // reports*/
    uint16_t start_address : 11; // Maximum report size of 256 bytes (as per the
                                 // spec).
} location_t;

// Defines where to extract the various controls of the standard CRC definition
// of a remote
//   inside the USB report
typedef struct {
    location_t colors[4]; // left, down, right, up
    location_t select;
    location_t start;
    location_t joystick_press[2]; // left, right
    location_t top_trigger[2]; // left, right
    location_t logo;
    location_t hat;
    location_t joystick[2][2]; // { left, right } x { X, Y }
    location_t triggers[2]; // left, right
    bool hat_angular;
    bool
        use_reports; // For now there is only support for the PS3 use of reports
} format_t;

// Parse a HID report descriptor and output the format
// If the result of the function is false, the format is not valid and should
// not be read
// It is UB to point to NULL for the report or set a length that is
//   not the real length of the report.
bool parse_format(uint8_t* report, uint16_t len, format_t* format);
#ifdef __cplusplus
}
#endif
#endif // HIDPARSER_H
