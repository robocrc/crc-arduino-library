#include "hidparser.h"
#include <limits.h>
#include <string.h>

#define min(a, b)                                                              \
    ({                                                                         \
        __typeof__(a) _a = (a);                                                \
        __typeof__(b) _b = (b);                                                \
        _a < _b ? _a : _b;                                                     \
    })

#define USAGE_MAX_COUNT 16
#define MAX_BUTTONS 17
#ifdef DEBUG
#include <stdio.h>
#endif
// #define DEBUG

// Relevant usage pages. Technically 16 bits, but since no usage page is defined
// with a
//   usage page greater than 0xFF, it is not really necessary.
const uint8_t USAGE_PAGE_GENERIC         = 0x01;
const uint8_t USAGE_PAGE_GAME_CONTROL    = 0x05;
const uint8_t USAGE_PAGE_GENERIC_CONTROL = 0x06;
const uint8_t USAGE_PAGE_LED             = 0x08;
const uint8_t USAGE_PAGE_BUTTON          = 0x09;

// Relevant usage IDs. Technically 16 bits, but since no usages are defined with
// an ID
//   greater than 0xB8, it is not really necessary.
const uint8_t USAGE_ID_GENERIC_JOYSTICK = 0x04;
const uint8_t USADE_ID_GENERIC_GAMEPAD  = 0x05;
const uint8_t USAGE_ID_GENERIC_X        = 0x30;
const uint8_t USAGE_ID_GENERIC_Y        = 0x31;
const uint8_t USAGE_ID_GENERIC_Z        = 0x32;
const uint8_t USAGE_ID_GENERIC_Rx       = 0x33;
const uint8_t USAGE_ID_GENERIC_Ry       = 0x34;
const uint8_t USAGE_ID_GENERIC_Rz       = 0x35;
const uint8_t USAGE_ID_GENERIC_HAT      = 0x39;
const uint8_t USAGE_ID_GENERIC_START    = 0x3D;
const uint8_t USAGE_ID_GENERIC_SELECT   = 0x3E;
// UP = 0, DOWN = 1, RIGHT = 2, LEFT = 3
const uint8_t USAGE_ID_GENERIC_DPAD = 0x90;
// TODO?
// const uint8_t USAGE_ID_GAMEPAD_FIRE     = 0x37;
// const uint8_t USAGE_ID_GAMEPAD_TRIGGER  = 0x39;

// Units definition. Only the angular payload is intersting for the hat control.
const uint32_t PAYLOAD_ANGULAR = 0x14;

// The size field of an HID item
typedef enum {
    SIZE_0 = 0b00,
    SIZE_1 = 0b01,
    SIZE_2 = 0b10,
    SIZE_4 = 0b11,
} item_size_t;

// The tag & type field of an HID item.
// Tags are dependent on types, so there's no point in separating them
typedef enum {
    // Main items
    INPUT_         = 0b100000,
    OUTPUT_        = 0b100100,
    COLLECTION     = 0b101000,
    FEATURE        = 0b101100,
    END_COLLECTION = 0b110000,

    // Global items
    USAGE_PAGE    = 0b000001,
    LOGICAL_MIN   = 0b000101,
    LOGICAL_MAX   = 0b001001,
    PHYSICAL_MIN  = 0b001101,
    PHYSICAL_MAX  = 0b010001,
    UNIT_EXPONENT = 0b010101,
    UNIT          = 0b011001,
    REPORT_SIZE   = 0b011101,
    REPORT_ID     = 0b100001,
    REPORT_COUNT  = 0b100101,
    PUSH          = 0b101001,
    POP           = 0b101101,

    // Local items
    USAGE          = 0b000010,
    USAGE_MINIMUM  = 0b000110,
    USAGE_MAXIMUM  = 0b001010,
    DESIGNATOR_IDX = 0b001110,
    DESIGNATOR_MIN = 0b010010,
    DESIGNATOR_MAX = 0b010110,
    STRING_IDX     = 0b011110,
    STRING_MIN     = 0b100010,
    STRING_MAX     = 0b100110,
    DELIMITER      = 0b101010,
} item_tag_t;

// An HID item. The size of the payload is determined by the _size field.
// A bit field is not used because this would slow down parsing and only
//   cost an extra byte of RAM ever (items are parsed one at a time)
typedef struct {
    item_size_t _size;
    item_tag_t tag;
    uint32_t payload;
} item_t;

// The global state of the HID parser, with fields needed for the CRC parser.
// See spec at http://www.usb.org/developers/hidpage/HID1_11.pdf
// Ignore physical min & max, unit exponent and convert unit to a bool to known
// if the
//   unit is a rotation unit.
typedef struct {
    uint32_t usage_page;
    int32_t logical_min;
    int32_t logical_max;
    uint32_t report_size;
    uint32_t report_id;
    uint32_t report_count;
    bool angular;
} state_t;

// The local state of the HID parser, with only the fields needed for the CRC
// parser. See spec at http://www.usb.org/developers/hidpage/HID1_11.pdf Ignore
// designators, strings & delimiter (TODO: are the useful?) Usages can't be used
// with usage_min and usage_max, so make it an union
typedef struct {
    uint8_t usage_count; // Number of usages filled. 0xFF means usage min & max
                         // rather than list
    union {
        uint32_t usage[USAGE_MAX_COUNT];
        struct {
            uint32_t usage_min;
            uint32_t usage_max;
        };
    };
} local_state_t;

// CRC only parser data.
typedef struct {
    bool use_reports; // Does the USB use reports?
    bool hat_angular; // Is the hat using angles?
    uint8_t buttons_defined; // The number of buttons parsed
    location_t buttons[MAX_BUTTONS];
    location_t hat;
    location_t select;
    location_t start;
    location_t joystick[6]; // X Y Z Rx Ry Rz
    location_t triggers[2];
    union {
        uint8_t report; // If there is a single report, keep track of the number
                        // of bits used.
        uint8_t reports[USAGE_MAX_COUNT]; // If there are many reports, keep
                                          // track for all of them.
    };
} parse_state_t;

// Return the number of bytes used in the item's payload.
int8_t item_size(item_t item)
{
    switch (item._size) {
    case SIZE_0: return 0;
    case SIZE_1: return 1;
    case SIZE_2: return 2;
    case SIZE_4: return 4;
    default: return 0;
    }
}

// Get the next item from the report.
// TODO: remove prints on arduino
item_t parse_item(uint8_t** report, uint16_t* len)
{
    uint8_t prefix = **report;
    item_t item    = {
        ._size = ((item_size_t)(prefix & 0b11)),
        .tag   = ((item_tag_t)((prefix & 0b11111100) >> 2)),
    };
    for (int i = item_size(item); i > 0; --i) {
        item.payload = (item.payload << 8) | *(*report + i);
    }
    uint8_t diff = 1 + item_size(item);
    *len -= diff;
    *report += diff;
    return item;
}

// Is the following usage page supported?
bool valid_usage_page(uint16_t usage_page)
{
    return usage_page == USAGE_PAGE_GENERIC
        || usage_page == USAGE_PAGE_GAME_CONTROL
        || usage_page == USAGE_PAGE_GENERIC_CONTROL
        || usage_page == USAGE_PAGE_LED || usage_page == USAGE_PAGE_BUTTON;
}

#ifdef DEBUG
// Helper to print a button. For debugging purposes only.
void print_location(const char* msg, location_t location)
{
    if (location.exist) {
        printf("%s: ", msg);
        printf("Relative: %d, size: %d, report: %d, start_address: %d\n",
            location.relative, 1 << location.size_, /*location.report*/ 0,
            location.start_address);
    }
}
#endif // DEBUG

// Try to find the next location which could potentially be a trigger
// This basically means to find a "button" with a size more than 1 bit width
location_t find_trigger(parse_state_t* parse_state)
{
    for (uint8_t i = 0; i < MAX_BUTTONS; i++) {
        location_t but = parse_state->buttons[i];
        if (!but.exist)
            continue;
        if (but.size_ > 0) {
            parse_state->buttons[i].exist = false;
#ifdef DEBUG
            print_location("state: ", parse_state->buttons[i]);
            print_location("but: ", but);
#endif
            return but;
        }
    }
    return (location_t) { .exist = false, 0 };
}

// Find the next button in order. Save the previous position to avoid searching
//   the already checked search space.
location_t next_button(parse_state_t* parse_state, uint8_t* location)
{
    for (; *location < MAX_BUTTONS; (*location)++) {
        location_t but = parse_state->buttons[*location];
        if (but.exist) {
            parse_state->buttons[*location].exist = false;
            (*location)++;
            return but;
        }
    }
    return (location_t) { .exist = false, 0 };
}

// Convert the parse state to the final format. This is needed because all
//   gamepads do not respect the same set of inputs, so by having all the
//   declared inputs before allocating them, we can achieve something that
//   seems more logical.
void finish(format_t* format, parse_state_t* parse_state)
{
    *format                = (format_t) { 0 };
    format->hat_angular    = parse_state->hat_angular;
    format->use_reports    = parse_state->use_reports;
    format->joystick[0][0] = parse_state->joystick[0];
    format->joystick[0][1] = parse_state->joystick[1];
    format->joystick[1][0] = parse_state->joystick[2];
    format->joystick[1][1] = parse_state->joystick[3];
    // If not all joystick are found, try with the two extras.
    for (uint8_t i = 0; i < 2; i++) {
        for (uint8_t j = 0; j < 2; j++) {
            if (!format->joystick[i][j].exist) {
                for (uint8_t k = 5; k < 6; k++) {
                    if (parse_state->joystick[k].exist) {
                        format->joystick[i][j] = parse_state->joystick[k];
                        parse_state->joystick[k].exist
                            = false; // No longer available
                    }
                }
            }
        }
    }
    format->hat = parse_state->hat;
    // If not hats where found, try to find 4 1-bit buttons side by side to
    // emulate it
    if (!format->hat.exist) {
        for (uint8_t i = 0; i < MAX_BUTTONS - 4; i++) {
            bool found    = true;
            uint8_t start = parse_state->buttons[i].start_address;
            for (uint8_t j = 0; j < 4; j++) {
                location_t but = parse_state->buttons[i + j];
                if (!but.exist || but.start_address != start + j
                    || but.size_ != 0) {
                    found = false;
                    break;
                }
            }
            if (found) {
                format->hat = parse_state->buttons[i];
                for (short j = i; j < i + 4; j++) {
                    parse_state->buttons[j].exist = false;
                }
                format->hat.size_   = 2;
                format->hat_angular = false;
                break;
            }
        }
    }
    format->triggers[0] = parse_state->triggers[0];
    format->triggers[1] = parse_state->triggers[1];
    if (!format->triggers[0].exist) {
        format->triggers[0] = find_trigger(parse_state);
    }
    if (!format->triggers[1].exist) {
        format->triggers[1] = find_trigger(parse_state);
    }
    format->select = parse_state->select;
    format->start  = parse_state->start;
    // Allocate remaining buttons. Since the usb standard gives no clue
    //   about what a button is, the order is that of the Logitech F130.
    // This means the buttons will probably be out of place for other
    //   non-PS USB HID controllers.
    uint8_t location = 0;
    for (uint8_t i = 0; i < 4; i++) {
        format->colors[i] = next_button(parse_state, &location);
    }
    format->top_trigger[0] = next_button(parse_state, &location);
    format->top_trigger[1] = next_button(parse_state, &location);
    if (!format->triggers[0].exist) {
        format->triggers[0] = next_button(parse_state, &location);
    }
    if (!format->triggers[1].exist) {
        format->triggers[1] = next_button(parse_state, &location);
    }
    format->select            = next_button(parse_state, &location);
    format->start             = next_button(parse_state, &location);
    format->joystick_press[0] = next_button(parse_state, &location);
    format->joystick_press[1] = next_button(parse_state, &location);
    format->logo              = next_button(parse_state, &location);
}

// Save a field (a specific input) to the parse state.
// Since it is valid to send multiple usages per Input item, or a usage_min
//   and usage_max, this might be called more than one time per Input item
bool add_field(
    uint32_t usage, state_t* state, parse_state_t* parse_state, bool relative)
{
    // The upper 16 bits represent the usage page. Technically, usage >> 16
    //   would work, but it seems clearer to indicate the chosen bits.
    uint16_t usage_page = (usage & 0xFFFF0000) >> 16;
    // The usage did not specify a usage page, so as per the spec, resort to
    //   the declared usage page.
    if (usage_page == 0) {
        usage_page = state->usage_page;
    }
    // Only keep the lower 16 bits
    usage &= 0xFFFF;
    // The number of reports is too high for what's supported. Should really
    //   never happen, but just in case. TODO: do we want to proceed with
    //   the parse instead of aborting?
    if (state->report_id > USAGE_MAX_COUNT)
        return false;
    uint8_t report_id     = state->report_id;
    uint8_t size          = state->report_size;
    uint8_t start_address = (report_id == 0)
        ? parse_state->report
        : parse_state->reports[report_id - 1];
    if (report_id == 0) { // Reports are not used by this device
        parse_state->use_reports = false;
        parse_state->report += size;
    } else {
        parse_state->use_reports = true;
        parse_state->reports[report_id - 1] += size;
    }
    uint8_t s = ffs(size);
    if (s == 0) {
        return false; // Zero width input?
    }
    s -= 1; // Real value is minus one
    if (size != (1 << s)) {
        return false; // More than one bit set?
    }
    location_t location = { .exist = true,
        .relative                  = relative,
        .size_                     = s,
        // .report                    = report_id,
        .start_address = start_address };
    if (usage_page == USAGE_PAGE_BUTTON
        /*|| (usage_page == USAGE_PAGE_GAME_CONTROL
            && usage == USAGE_ID_GAMEPAD_FIRE)
        || (usage_page == USAGE_PAGE_GAME_CONTROL
            && usage == USAGE_ID_GAMEPAD_TRIGGER)*/ // TODO?
          // TODO: should probably be handled separately? 
          || (usage_page == USAGE_PAGE_GENERIC
            && (usage ^ USAGE_ID_GENERIC_DPAD) < 3)) {
#ifdef DEBUG
        printf("It's a button\n");
#endif
        // Skip if we already have all buttons
        if (parse_state->buttons_defined + 1 == MAX_BUTTONS) {
#ifdef DEBUG
            printf("Already used all buttons\n");
#endif
            return true;
        }
        parse_state->buttons[parse_state->buttons_defined] = location;
        parse_state->buttons_defined++;
    } else if (usage_page == USAGE_PAGE_GENERIC
        && usage == USAGE_ID_GENERIC_SELECT) {
        parse_state->select = location;
    } else if (usage_page == USAGE_PAGE_GENERIC
        && usage == USAGE_ID_GENERIC_START) {
        parse_state->start = location;
    } else if (usage_page == USAGE_PAGE_GENERIC && usage >= USAGE_ID_GENERIC_X
        && usage <= USAGE_ID_GENERIC_Rz) {
#ifdef DEBUG
        printf("It's a joystick axis\n");
#endif
        uint8_t axis = usage - USAGE_ID_GENERIC_X; // Get the number of the axis
        parse_state->joystick[axis] = location;
    } else if (usage_page == USAGE_PAGE_GENERIC
        && usage == USAGE_ID_GENERIC_HAT) {
#ifdef DEBUG
        printf("It's a hat\n");
#endif
        parse_state->hat_angular = true;
        parse_state->hat         = location;
    }
    return true;
}

// Parse the state and set the next input(s). Returns true if the parsing should
// abort
bool input(state_t* state,
    local_state_t* local_state,
    parse_state_t* parse_state,
    bool relative)
{
    if (!valid_usage_page(state->usage_page)) {
#ifdef DEBUG
        printf("Invalid usage page: 0x%X\n", state->usage_page);
#endif
        return false; // It is only unsupported, the parser should not abort
    }

    if (local_state->usage_count == 0xFF) {
        for (uint32_t usage = local_state->usage_min;
             usage <= local_state->usage_max; usage++) {
            if (!add_field(usage, state, parse_state, relative)) {
                return true;
            }
        }
    } else {
        for (uint8_t i = 0; i < state->report_count; i++) {
            uint8_t idx    = min(i, local_state->usage_count);
            uint32_t usage = local_state->usage[idx];
            if (!add_field(usage, state, parse_state, relative)) {
                return true;
            }
        }
        local_state->usage[0]
            = local_state->usage[local_state->usage_count - 1];
        local_state->usage_count = 1;
    }
    return false;
}

// Set the value of the field according to the item payload & size
// In other words, set only the bits from the tag in the value.
void set(uint32_t* v, item_t item)
{
    uint8_t bits       = 8 * item_size(item);
    uint32_t keep_mask = UINT32_MAX << bits;
    uint32_t new_mask  = (1 << bits) - 1;
    *v                 = (*v & keep_mask) | (item.payload & new_mask);
}

// Parse a HID report descriptor and output the format
// If the result of the function is false, the format is not valid and should
// not be read It is UB to point to NULL for the report or set a length that is
// not the real length of
//   the report.
bool parse_format(uint8_t* report, uint16_t len, format_t* format)
{
    state_t state             = {};
    local_state_t local_state = {};
    parse_state_t parse_state = {};

    // parse each item, one at a time
    while (len != 0) {
        item_t item = parse_item(&report, &len);
#ifdef DEBUG
        printf("size: %d, tag: 0x%02X, payload: 0x%X\n", item._size, item.tag,
            item.payload);
#endif
        switch (item.tag) {
        // Main items
        case INPUT_:
            // The bit 2 indicates if the data is "relative" — that each update
            // is
            //   a difference with the previous state (like with a mouse).
            // Gamepad shouldn't have relative joystick, but just in case.
            // Because we want the relative variable to contain only 0 or 1 for
            // the
            //   bitfield initialisation, explicitly compare it to 0.
            {
                bool relative = ((item.payload & 0b100) != 0);
                input(&state, &local_state, &parse_state, relative);
            }
            local_state = (local_state_t) {};
            break;
        case OUTPUT_:
            local_state = (local_state_t) {};
            // TODO?
            break;
        case COLLECTION:
            local_state = (local_state_t) {};
            // Nothing to do
            break;
        case FEATURE:
            local_state = (local_state_t) {};
            // TODO: read spec
            break;
        case END_COLLECTION: local_state = (local_state_t) {}; break;

        // Global items
        case USAGE_PAGE: set(&state.usage_page, item); break;
        case LOGICAL_MIN: set((uint32_t*)&state.logical_min, item); break;
        case LOGICAL_MAX: set((uint32_t*)&state.logical_max, item); break;
        case PHYSICAL_MIN:
        case PHYSICAL_MAX:
        case UNIT_EXPONENT:
            // Don't care
            break;
        // Check if the unit says its a angular rotation (for the directions)
        case UNIT: state.angular = (item.payload == PAYLOAD_ANGULAR); break;
        case REPORT_SIZE: set(&state.report_size, item); break;
        case REPORT_ID: set(&state.report_id, item); break;
        case REPORT_COUNT: set(&state.report_count, item); break;
        case PUSH:
        case POP:
            // TODO: Should we implement a stack to be fully compliant or should
            // we save RAM space?
            return false;

        // Local items
        case USAGE:
            // Trying to send usage and usage_min and max at the same time is
            // invalid
            if (local_state.usage_count == 0xFF)
                return false;
            set(&local_state.usage[local_state.usage_count], item);
            local_state.usage_count++;
            break;
        case USAGE_MINIMUM:
            // Trying to send usage and usage_min and max at the same time is
            // invalid
            if (local_state.usage_count != 0 && local_state.usage_count != 0xFF)
                return false;
            local_state.usage_count = 0xFF;
            set(&local_state.usage_min, item);
            break;
        case USAGE_MAXIMUM:
            // Trying to send usage and usage_min and max at the same time is
            // invalid
            if (local_state.usage_count != 0 && local_state.usage_count != 0xFF)
                return false;
            local_state.usage_count = 0xFF;
            set(&local_state.usage_max, item);
            break;
        case DESIGNATOR_IDX:
        case DESIGNATOR_MIN:
        case DESIGNATOR_MAX:
        case STRING_IDX:
        case STRING_MIN:
        case STRING_MAX:
        case DELIMITER:
            // TODO?
            break;
        default:
#ifdef DEBUG
            printf("Invalid tag\n");
#endif
            return false;
        }
    }
    finish(format, &parse_state);
    return true;
}
